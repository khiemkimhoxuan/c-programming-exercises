
Oblig 1 er levert, og vi har gjort oss ferdig med det grunnleggende i C. Resten av kurset består av mye teori, men det blir også litt mere C; hovedsakelig i forbindelse med nettverk og kommunikasjon mellom prosesser. Jeg fortsetter å legge ut teorioppgavene fra tidligere år.

Det kan være lurt å gå tilbake og titte litt mer på de ukenesoppgavene man ikke har rukket å lese, selv om det naturligvis ikke er nødvendig å løse alle. Forrige uke var mange opptatt med obligen, men jeg la også ut noen få oppgaver om debugging som kan være verdt en titt.

Denne uken er det egentlig bare teorioppgaver, men jeg legger likevel med en oppgave for meg selv, som jeg kommer til å legge ut en løsning på, og eventuelt drøfte nærmere i gruppetimen. Jeg skal arbeide videre med programmet `gull` som fikk debugging-støtte [sist uke](https://github.com/INF1060H11/oppgaver/tree/master/uke38forslag/gull), og..

- Bruke ringbufferet vi lagde i uke 37 for å lagre gullkornene (`rbuf.h`; ligger allerede i mappen)
- Lage et nytt menyvalg der brukerne av programmet kan legge til sine egne gullkorn
- Se til at programmet bruker minne på en fornuftig måte med `valgrind` eller et tilsvarende verktøy.



### Teori-oppgaver for uke 39:

#### 1:

Beskriv hva som skjer når en bruker gjør et tastetrykk på en terminal knyttet til et flerbruker-system. Svarer systemet forskjellig for preemptive eller ikke-preemptive systemer? Hvorfor eller hvorfor ikke?


#### 2:

Tidligere versjoner av Windows brukte en essensielt ikke-preemptive dispatching-teknikk som Microsoft kaller "cooperative multitasking".  Med denne teknikken, forventes det at hvert program frivillig gir fra seg CPU-en periodisk for å gi andre prosesser en sjanse til å eksekvere. Diskuter denne metoden. Hvilke potensielle vanskeligheter kan denne metoden forårsake?


#### 3:

Hvorfor er program-relokering unødvendig når virtuelt minne benyttes?
 

#### 4:

CPU schedulerings-algoritmen (i Unix) er en enkel prioritets-algoritme.  Prioriteten for en prosess beregnes som forholdet mellom CPU tiden som er brukt av prosessen og virkelig tid som har gått. Jo lavere tall jo høyere prioritet. Prioriteter re-kalkuleres hvert tiende sekund.

1. Hva slags jobber favoriseres ved denne typen algoritme?
2. Hvis det ikke utføres noe I/O, vil denne algoritmen reduseres til
   en round-robin-algoritme. Forklar hvorfor.
3. Diskuter denne algoritmen i forhold til de generelle hensiktene som
   man ønsker å oppnå med schedulering.


#### 5:

I inntrengerprogrammer finner man ofte følgende lille kodesnutt flere
steder:

    if (fork() > 0) exit(0);

Hva gjør denne kodebiten? Hvorfor brukes den i ulovlige programmer?


#### 6:

Forklar konsekvensene av at en bruker utfører:

    while(1) fork();

Hint: slå opp "fork bomb"
