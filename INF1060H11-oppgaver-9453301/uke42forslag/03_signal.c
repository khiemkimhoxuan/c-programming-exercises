

#include <sys/types.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

void sigint()
{ 		
	signal(SIGINT, sigint); /* NOTE some versions of UNIX will reset 
				 * signal to default after each call. So for 
				 * portability reset signal each time */
 
	printf("Child received SIGINT. Exiting..\n");
    exit(0);
}




int main(int argc, const char *argv[])
{
    pid_t pid;

    if ((pid = fork()) == -1) {
        perror("fork call failed");
        exit(1);
    }
    
    if (pid == 0) { /* child */
        signal(SIGINT, sigint);
        printf("Child process is waiting..\n");
        while (1) sleep(1);

    } else { /* parent */
        sleep(1);
        printf("Parent is messaging child..\n");
        kill(pid, SIGINT);
        printf("Parent is waiting for child..\n");
        wait();
        printf("Parent is done waiting.\n");
    }
    return 0;
}

