
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>

#define MSGSIZE 20

main()
{  
    char inbuf[MSGSIZE];
    int p[2];
    pid_t pid;

    /* open pipe */ 	
    if (pipe(p) == -1) {
        perror("pipe call failed");
        exit(1);
    }
    printf("Opened pipe. Forking..\n");

    switch( pid = fork() ) {
        case -1:
            perror("fork call failed");
            exit(2);

        case 0: /* child */
            close(p[0]);
            write(p[1], "Hey ho, let's go!", MSGSIZE);
            printf("Message sent from child.\n");
            break;

        default: /* parent */
            close(p[1]);
            read(p[0], inbuf, MSGSIZE);
            printf("Message received by parent.\n");
            printf("Message: %s\n", inbuf);
            wait(0);
    }
    exit(0);
}

