

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>


int main(int argc, const char *argv[])
{
    if (fork() == 0) {

        fork(); fork(); fork();

        usleep(100000);
        printf("1\n");
        usleep(100000);
        printf("2\n");
        usleep(100000);
        printf("3\n");
        
        wait();
        exit(0);
    }
    wait();
    printf("farvel!\n");
    return 0;
}
