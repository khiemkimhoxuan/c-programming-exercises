
### Løsningsforslag uke 43: Teorioppgaver om filsystem og disk


#### 1:

A)

FCFS: 99 55 43 78 4 11 89 67 1 98 45 88 60 30 77 
SCAN: 55 60 67 77 78 88 89 98 99 45 43 30 11 4 1


#### 2:

Det opprinnelige problemet var at filene dine var fragmenterte, og at det å få tak i dem krevde mange disksøk for å lese blokker som var fordelt ut over hele disken. Ved å kopiere over på en annen disk ble dette problemet løst, fordi den store ledige kontinuerlige diskplassen på den nye disken tillot hver fil å bli lagret kontinuerlig.


#### 3:

Når en fil flyttes fra en disk til en annen må blokkene fysisk kopieres over til den andre disken. Når den flyttes fra ett sted til et annet i filstrukturen på den samme disken trenger man imidlertid ikke å flytte dataene. Det er tilstrekkelig å identifisere den nye logiske lokasjonen til filen, og oppdatere filkatalogen.


#### 4:

Enhver fil som skal kunne leses helhetlig fra begynnelse til slutt krever sekvensiell aksess. Eksekverbare filer vil bli aksessert sekvensielt, likeledes filer med kildekode under kompileringsprosessen.

Filer som skal håndtere forespørsler som kommer inn i tilfeldig rekkefølge må ha random aksess. F.eks filer med oversikt over kontobalanser i en bank eller en database.


#### 5:

Da de fleste systemer krever en betydelig mengde plass til indekser som peker til blokker på en disk er det ønskelig å begrense størrelsen på indeksen mest mulig. En måte å gjøre dette på er å slå flere blokker sammen i clustre. Siden et cluster da vil være den minste lagringsenheten som kan tilordnes en fil, vil små filer kaste bort mye lagringsplass når clustrene inneholder et stort antall blokker. Dersom disken er partisjonert vil det totale antall blokker i en partisjon bli færre, og man kan derfor også la clusterstørrelsen bli mindre slik at diskplassen kan utnyttes bedre. Merk at partisjonering også brukes for Å støtte flere ulike operativsystemer, eller flere ulike filsystemer.


#### 6:

Open allokerer bufferplass i minnet til read og write operasjonene.  Close frigir denne bufferplassen igjen. På noen systemer benyttes disse operasjonene til å sikre fillintegritet. Dersom en prosess har åpnet en fil for skriving vil ingen andre filer kunne åpne den til noe annet enn lesing.
