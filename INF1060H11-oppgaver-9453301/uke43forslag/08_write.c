#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main () {
	/* open() requires at least 2 arg. You can read more about unix io here:
	 * http://rabbit.eng.miami.edu/info/functions/unixio.html
	 */	
	int f = open("test2.txt", O_WRONLY | O_CREAT | O_TRUNC);

	if (-1 == f) { // open() returns -1 if it failed.
		printf("File couldn't be opened!\n");
		exit(2);
	}

	char *msg = "hello world";
	int bytes = write(f, msg, strlen(msg));

	/* write() returns amount of bytes it did actually write to the stream/file.
	 * If it returns -1, then it failed. */
	if (-1 == bytes) {
		printf("Writing to file failed!\n");
	} else {
		printf("%d bytes written to file.\n", bytes);
	}
	close(f); // Must not forget to close the file.
	return 0;
}
