
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "file.h"
#include "debug.h"

char *File_get_line(FILE *fd)
{
    char *result;
    size_t length;

    result = NULL;

    if (getline(&result, &length, fd) != -1) {
      result[strlen(result)-1] = '\0'; /* chop off \n */
      return result;
    } else
      return NULL;
}

int File_path_exists(char* path)
{
    int exists;
    FILE* fd;
    fd = fopen(path, "r");
    exists = (fd != NULL);
    if (exists) fclose(fd);
    return exists;
}

