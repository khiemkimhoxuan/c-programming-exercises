

#ifndef GEM_H
#define GEM_H

#include "rbuf.h"       /* ring buffer queue */

#define GEM_LIMIT   40  /* max gem count */
#define GEM_MAXLEN  500 /* max gem length (chars) */

#define GEMLIST_PATH "./gems.lst"

char   *Gem_get_random();
int     Gem_write(rbuf_t *gems, char *path);
rbuf_t *Gem_read_or_create(char *path);

#endif
