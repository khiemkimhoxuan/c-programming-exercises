
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "gem.h"
#include "file.h"


char *Gem_get_random()
{
    srand(time(NULL));
    switch(rand() % 6) {
      case 0: return("Bli og glad, hipp hurra!");
      case 1: return("Ut på tur, aldri sur!");
      case 2: return("Evig optimist!");
      case 3: return("Tommelen opp!");
      case 4: return("Hurra!");
      case 5: return("Tommelen opp, dette er topp!");
      default: return("ERROR!");
    }
}

int Gem_write(rbuf_t *gems, char *path)
{
    FILE *fd;
    int i;
    if ((fd = fopen(path, "w")) != NULL)
    {
      for(i=0; i<gems->size; i++)
        fprintf(fd,"%s\n", (char *) Rbuf_peek(gems, i));
      fclose(fd);
      return 1;
    }
    return -1;
}

static rbuf_t *Gem_read(char *path)
{
    FILE *fd;
    char *str;
    rbuf_t *gems;
    gems = Rbuf_new(GEM_LIMIT);

    if ((fd = fopen(path, "r")) != NULL)
    {
      while((str = File_get_line(fd)) != NULL)
        Rbuf_put(gems, str);
      fclose(fd);
    } else
      perror("Error reading gems from disk");
    return gems;
}

rbuf_t *Gem_defaults()
{
    rbuf_t *gems;
    gems = Rbuf_new(GEM_LIMIT);
    Rbuf_put(gems, "Bli og glad, hipp hurra!");
    Rbuf_put(gems, "Ut på tur, aldri sur!");
    Rbuf_put(gems, "Evig optimist!");
    Rbuf_put(gems, "Tommelen opp!");
    Rbuf_put(gems, "Hurra!");
    Rbuf_put(gems, "Tommelen opp, dette er topp!");
    return gems;
}

rbuf_t *Gem_read_or_create(char *path)
{
    if (File_path_exists(path)) {
      return Gem_read(path);
    } else
      return Gem_defaults();
}
