
/*  A simple gem server.
 */


#include <netinet/in.h> /* address structs */
#include <sys/socket.h> /* socket functions and structs */
#include <netdb.h>      /* address translation */

#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include <unistd.h> /* syscall wrappers */

#include "rbuf.h"
#include "gem.h"

#define SRV_PORT 5445

void Server_accept_loop(int req_sock)
{
    struct sockaddr_in addr;
    int addrlen, sock;
    pid_t pid;
    char *gem;
    rbuf_t* gems;
    gems = Gem_read_or_create(GEMLIST_PATH);

    while (1)
    {
      sock = accept(req_sock, (struct sockaddr *) &addr, 
                                            &addrlen);
      printf("Send gem: %d\n", sock);
      gem = (char *) Rbuf_random(gems);
      write(sock, gem, strlen(gem));
      close(sock);
    }
}

void Server_listen(int port)
{
    struct sockaddr_in addr; 
    int req_sock;

    req_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    bzero((void *) &addr, sizeof(addr));
    addr.sin_family = AF_INET; 
    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_port = htons(port);

    bind(req_sock, (struct sockaddr *) &addr, sizeof addr);
    listen(req_sock, SOMAXCONN);

    printf("Listening at port %d..\n", port);
    Server_accept_loop(req_sock);
    close(req_sock);
}


main(void)
{
    Server_listen(SRV_PORT);
    return 0;
} 

