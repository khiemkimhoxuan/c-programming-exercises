#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>

int main() {
	int p = fork(); // Make a child process.
	int child = (p == 0); // 1 if we are in child, 0 if we are in parent

	FILE *f = fopen ("test.txt","w");

	if (f == NULL) {
		printf ("pid %d: Kunne ikke åpne filen!\n", getpid());
		exit(2);
	}

	if (child) { // True only if we are in child
		char *msg = "I'm a child!\n";

		fseek(f, 0L, SEEK_END); // Move "pointer" to the end of the file
		fwrite(msg, strlen(msg), sizeof(char), f); // Write the msg
		fclose(f); // ...and close the file

	} else { // True only if we are in parent
		char *msg = "I'm a parent!\n";

		/* These are exactly the same as in child: */
		fseek(f, 0L, SEEK_END);
		fwrite(msg, strlen(msg), sizeof(char), f);
		fclose(f);

		printf("Waiting for child...\n");

		int status;
		wait(&status); // Wait for child process to return.
	}

	return 0;

	/* Why doesn't it work like we want? One possible reason is that 
	 * one of the processes finishes and closes the file before the 
	 * other one even began writing. The other possibility is that 
	 * one of the processes writes a message after the other did 
	 * fseek, but before it wrote anything. The funny thing is that 
	 * sometimes it works, sometimes it's doesn't - it's random. 
	 */
}
