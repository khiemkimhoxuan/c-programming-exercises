

### Suggested solutions, week 45: Networks

#### 5:

For example: the receiver sends a confirmation for all packets that he has back to the server. The Sender resends all packet for which he has not received a confirmation without one seconds.

#### 6:

In the proposed solution for question 2, order is not guaranteed. The simplest way for ensuring order is, not to send the next packet before the previous one has been confirmed. However, this is very inefficient.

Other protocols, e.g. such that use a sliding window, can be imagined.

#### 7:

    kilobit =
    (10^3)^1 bit = 1000 bit
    megabit =
    (10^3)^2 bit = 1000000 bit


A)

The time for putting the entire packet onto the channel is 1000 bit/10000000 bit/s = 100 microseconds. The time a bit needs for travelling the length of the channel is:

    2000 km/300.000 km/s = 6700 microseconds
    Total 6800 microseconds.

B)

The time for putting the entire packet onto the channel is:

1000 bit/10000000000 bit/s = 0.1 microseconds.
The time a bit needs for travelling the length of the channel is:

2000 km/300.000 km/s = 6700 microseconds
Total 6700.1 microseconds.

#### 8:

The question is misleading. It is not the speed of the channel that has increased, but its bandwidth, or its throughput. You can in fact send 1000 times more data, but you can't send 1000 times faster. You can only put the data 1000 times faster onto the link. In terms of speed, the distance will nearly always dominate the delay.

#### 9:

TCP guarantees reliability and order, the protocol does not have to account for network problems. So, we can for example propose for chess:

    the client always uses white.
    the client draws first.
    the server always uses black.
    each move is encoded in 5 bytes.
    in a normal move.
    the first 2 bytes indicate the source field.
    the second 2 bytes indicate the destination field.
    the 5th byte is '-' for nothing special, 'c' for check, or 'm'. for checkmate.
    "queen" for castling queen side (lang rokade).
    "kind-" for castling king side (kort rokade).
    "-----" to give up.
    "?????" to offer a draw (remis).
    "aaaaa" to accept a draw.
    "ddddd" to decline a draw.
    the game ends in case of an accepted draw, in case that a player gives up, in case of a checkmate, or in case that a king has been hit.

#### 10:

A simple solution is to write a client that sends UDP data packet as quickly as possible to a server, and a server that counts the number of packets that it has received within a period of time. UDP is appropriate because:

- it does not add any flow control, congestion control, retransmission or reordering.
- the packets that arrive are exactly the packets that have been sent.

When such a test is performed, the bottleneck of the slowest part of the communication connection between sender and receiver is identified. This may be:

- a client processor that is too slow (unlikely).
- a receiver processor that is too slow (unlikely).
- the I/O bus of the sending machine (possible with Gigabit Ethernet).
- the I/O bus of the receiving machine (unlikely).
- the network card hardware (likely if you're alone on the net).
- the network connection (very likely in case of a shared network).

The measurement can also vary over time. If this happens and you are always the only user of client and server machine, it is a good indication that the network connection is the bottleneck.
