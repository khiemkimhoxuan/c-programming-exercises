
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "file.h"

/* See also './illustrasjoner/file.c'
 *
 */

char *File_get_line(FILE *fd, int maxlen)
{
    char *buf, *result;
    int len;

    result = NULL;
    buf = malloc(maxlen+1);

    if ((len = getline(&buf, &maxlen, fd)) != -1)
    {
        /* Here we chop off the '\n' too. */
        result = malloc(sizeof(char) * (len + 1));
        strcpy(result, buf);
        result[len-1] = '\0';
    }
    free(buf);
    return result;
}

/* NOTE: Getline will automatically reallocate more memory
 * to the buffer using `realloc()` as it is needed.
 *
 * Therefore, `File_get_line()` will still work if maxlen is
 * lower than the length of the line that is read. In such a
 * case, `getline()` will simply grow the buffer, and update
 * maxlen accordingly. This behaviour is documented in
 * `man getline`.
 *
 * The reason for allocating maxlen + 1 would otherwise
 * be to make space for a terminating null byte. Since
 * `getline()` already handles the allocation however,
 * it is a pretty useless detail here.
 *
 * The most obvious drawback in allocating the buffer on
 * each function call is the speed penalty. Two benefits
 * could be that it is "good enough and quick to write",
 * or that it would allow for arbitrary line lengths, had
 * we been using a more basic function than `readline()`.
 */

int File_path_exists(char* path)
{
    int exists;
    FILE* fd;
    fd = fopen(path, "r");
    exists = (fd != NULL);
    if (exists) fclose(fd);
    return exists;
}


