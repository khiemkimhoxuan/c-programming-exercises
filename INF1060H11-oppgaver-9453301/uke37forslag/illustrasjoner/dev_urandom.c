
#include <stdio.h>

int main(void)
{
    int num;
    FILE *fd;

    if ((fd = fopen("/dev/urandom", "r")) == NULL) {
        perror("Error opening /dev/urandom");
        return 1;
    }

    fread(&num, sizeof(num), 1, fd);

    printf("Tilfeldig tall: %u\n", num);

    return 0;
}

