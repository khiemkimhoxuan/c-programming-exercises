#include <stdio.h>



struct tree_node_s {
    struct tree_node_s *father, *mother;
    char *name;
};

typedef struct tree_node_s tree_node;






/* Print tree of ancestors for a node */
void print_tree(tree_node *node, int depth)
{
    int i;
    for (i=0; i<depth; i++) putchar(' ');
    printf("%s\n", node->name);
    if (node->mother != NULL) print_tree(node->mother, depth+2);
    if (node->father != NULL) print_tree(node->father, depth+2);
}



/* Set up some of Donald's ancestors */
int main(int argc, const char *argv[])
{
    tree_node root, father, mother, grandfather, grandmother;

    mother.father = mother.mother = NULL;
    father.father = father.mother = NULL;
    grandfather.father = grandfather.mother = NULL;
    grandmother.father = grandmother.mother = NULL;

    root.father   = &father;
    root.mother   = &mother;
    mother.mother = &grandmother;
    mother.father = &grandfather;

    root.name         = "Donald";
    mother.name       = "Lillegull";
    father.name       = "Didrik";
    grandfather.name  = "Fergus";
    grandmother.name  = "Myra";

    print_tree(&root, 0);

    return 0;
}


