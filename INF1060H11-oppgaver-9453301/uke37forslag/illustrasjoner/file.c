
#include <stdio.h>
#include <stdlib.h>

/* prints every second line of a file */

void print_some_lines(char *path)
{
    FILE *fd;
    int nbytes, print_this;
    char *buf;

    nbytes     = 10;
    print_this = 0;

    buf = malloc(sizeof(char) * (nbytes + 1));

    fd = fopen(path, "r");

    while (getline(&buf, &nbytes, fd) != -1) {
        if (print_this)
          printf("%s", buf);
        print_this = !print_this;
    }

    free(buf);
    fclose(fd);
}




int main(int argc, char *argv[])
{
    if (argc == 2) {
        print_some_lines(argv[1]);
    } else
        printf("Must be run with a file path as argument.\n");
  
    return 0;
}
