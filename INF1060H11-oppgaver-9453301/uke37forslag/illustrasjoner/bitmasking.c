

#include <stdio.h>

/* Bitwise operators:
 *
 *    ~   not     (negation)
 *    &   and     (both)
 *    |   or      (one or both)
 *    ^   xor     (only one)
 */



/* returns true if the bit at pos in c is set. */
int check_bit(char c, int pos)
{
    char mask;
    mask = 1 << (7-pos);
    return c & mask;
}

/* prints a char bit by bit */
void print_bits(char c)
{
    int i;

    for (i=0; i<8; i++)
        putchar(check_bit(c, i) ? '1' : '0');
    putchar('\n');
}



int main(int argc, const char *argv[])
{
    print_bits(0);
    print_bits(1);
    print_bits(2);
    print_bits(3);
    print_bits(4);
    print_bits('a');
    print_bits('?');
    
    return 0;
}
