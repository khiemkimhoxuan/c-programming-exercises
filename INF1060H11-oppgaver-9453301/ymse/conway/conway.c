
#include <stdio.h>
#include <stdlib.h>


#define WIDTH       160
#define HEIGHT      90

#define DEAD        0
#define ALIVE       1

#define LOWER       '.'
#define UPPER       '\''
#define BOTH        ':'


typedef char ** field_t;



/* FIELD */

field_t mkfield()
{
    field_t field;
    char *line;
    int i, j;

    field = (field_t) malloc(sizeof(line) * HEIGHT);

    for (i=0; i<HEIGHT; i++) {
        field[i] = malloc(sizeof(char) * WIDTH);
        for(j=0; j<WIDTH; j++)
            field[i][j] = DEAD;
    }
    return field;
}

int check(field_t field, int x, int y)
{
    return x >= 0 && y >= 0 && x < WIDTH && y < HEIGHT &&
        field[y][x];
}


int neighbours(field_t field, short x, short y)
{
    int count = 0;
    int i, j;

    for (i=-1; i<2; i++)
        for (j=-1; j<2; j++) {
            if (((i!=0) || (j!=0)) && check(field, x+i, y+j))
                count++;
        }
    return count;
}

void compute_generation(field_t a, field_t b)
{
    int i, j;

    for (i=0; i<HEIGHT; i++)
        for (j=0; j<WIDTH; j++) {
            switch(neighbours(a, j, i)) {
                case 3:
                    b[i][j] = ALIVE;
                    break;
                case 2:
                    if (a[i][j])
                        b[i][j] = ALIVE;
                    break;
                default:
                    b[i][j] = DEAD;
                    break;
            }
        }    
}

void import(field_t field, char *path)
{
    FILE *fd;
    int width, height, i, j, x_offset, y_offset;

    fd = fopen(path, "r");
    
    fscanf(fd, "%d,%d\n", &width, &height);

    x_offset = (WIDTH/2)  - (width/2);
    y_offset = (HEIGHT/2) - (height/2);

    for (i=0; i<height; i++) {
        for (j=0; j<width; j++)
            field[i + y_offset][j + x_offset] = (fgetc(fd) == 'X');
        
        fgetc(fd); /* skip newline */
    }
    fclose(fd);
}




/* GRAPHICS */

void reset_term()
{
    printf("\033[2J\033[1;1H");
}

void print_line()
{
    int i;
    for (i=0; i<WIDTH+2; i++)
        putchar('-');
    putchar('\n');
}

void render(field_t field)
{
    int i, j;
    reset_term();
    print_line();
    
    for (i=0; i<HEIGHT; i+=2) {
        putchar('|');
        for (j=0; j<WIDTH; j++) {
            if (field[i][j] && field[i+1][j])
                putchar(BOTH);
            else if (field[i][j])
                putchar(UPPER);
            else if (field[i+1][j])
                putchar(LOWER);
            else
                putchar(' ');
        }
        putchar('|');
        putchar('\n');
    }
    print_line();
}

void animate(field_t a)
{
    field_t b = mkfield();

    while (1) {
    
        render(a);
        usleep(10000);
        compute_generation(a, b);
    
        render(b);
        usleep(10000);
        compute_generation(b, a);
    }
}




int main(int argc, const char *argv[])
{
    field_t field = mkfield();

    import(field, "acorn.dish");

    animate(field);


    free(field);
    return 0;
}








