

### Løsningsforslag for teori-oppgaver, uke 39


#### 1:

Tastetrykk blir vanligvis håndtert av en interruptrutine som er 
uavhengig av den vanlige skedulering og dispatchingen. Når en bruker i 
et flerbrukersystem taster inn et tegn opplever systemet et avbrudd.  
Tastingen blir håndtert, og tegnet blir levert til den prosessen som 
skal ha det. Dersom inntastingen ikke skulle påvirke den prosessen som 
kjærer i øyeblikket blir kontroll over CPUen gitt tilbake til den 
kjørende prosessen umiddelbart.

#### 2:

Under ideelle forutsetninger vil denne metoden fungere fint. Dens
største mangel er at den ikke håndterer programmer som ikke vil
samarbeide, og programmer som inneholder feil (f.eks. evige løkker).
Den eneste måten å stanse en evig løkke på vil være å slå maskinen av
og på.

#### 3:

Relokering er nødvendig når et program ligger i en minnelokasjon som
er forskjellig fra den  minnelokasjonen som ble  antatt under
kompilering. Da må alle adressereferanser i binærkoden endres. I
virtuelt minne er alle adresser logiske og ikke fysiske, og dermed
blir oversettelsesprosessen som er beskrevet over gjort automatisk i
det prosessen får tildelt sider (pages) i minnet.


#### 4:

1. Denne algoritmen favoriserer jobber som krever mye I/O. Mens
   prosessene venter på I/O, går tiden uten at forbrukt CPU tid øker, 
   og dermed får prosessen høyere prioritet enn den ellers ville hatt.  
   Dersom den da trenger lite CPU tid før den igjen trenger I/O vil 
   den hver gang den er klar til å eksekvere på CPUen slippe til 
   nesten umiddelbart, og raskt gjøre seg ferdig med det den skal, før
   den igjen må blokkeres for å gjøre I/O.

2. Dersom hver prosess er begrenset av CPU bruk, vil prioriteten til
   en prosess reduseres umiddelbart når den blir kastet ut.  Dette 
   fordi den akkurat har brukt CPU tid, noe som øker dens "ratio".  På
   det tidspunktet vil den prosessen som har ventet lengst ha høyest 
   prioritet, siden dens "real time" er lengst i forhold til dens 
   forrige CPU tid. Videre vil lavest prioritet alltid bli gitt til 
   den prosessen som mest nylig fikk tildelt CPU tid, og dens 
   prioritet vil øke mens alle andre prosesser slipper til. Da er vi i 
   en situasjon hvor alle prosesser eksekverer etter tur, og altså 
   round robin.

3. Denne algoritmen kombinerer flere av kravene på en god måte.
   Sulting er umulig fordi prioriteten til en prosess stiger så lenge 
   den ikke har tilgang til CPU'en. Den balanserer ressurser ved at 
   den gir fordeler til I/O tunge jobber slik at de slipper til hver 
   gang de trenger det, og bruker den gjenværende tiden på CPU-tunge 
   jobber. Den gir forholdsvis konsistente responstider, og gir en 
   naturlig ytelsesdegradering pga. at den reduseres til round robin 
   på et gitt tidspunkt. Den møter imidlertid ikke krav til antall 
   jobber ferdigstilt eller minimalisering av responstid.


#### 5:

Koden kloner en barneprosess mens den selv dør. Resultatet av dette er at prosessen skifter PID; noe som gjør det vanskeligere å oppdage programmet og få drept det.


#### 6:

Dette er en "fork bomb", som konstant spaltes, og dermed starter en kjedereaksjon som beslaglegger systemressursene, med mindre det er lagt inn sperrer mot dette i systemet.
