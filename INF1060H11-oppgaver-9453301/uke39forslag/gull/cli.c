
#include <stdio.h>
#include "gem.h"
#include "file.h"
#include "rbuf.h"
#include "debug.h"



void Cli_print_gem(rbuf_t *gems)
{
    printf("%s\n", (char *) Rbuf_random(gems));
}

void Cli_flush_input()
{
    int c;
    while ((c = getchar()) != '\n' && c != EOF);
}

void Cli_menu_prompt()
{
    printf("[g]ullkorn [b]idra [a]vslutt :) > ");
}

char* Cli_prompt(char* msg)
{
    printf("%s > ", msg);
    return File_get_line(stdin, GEM_MAXLEN);
}

void Cli_contrib_gem(rbuf_t *gems)
{
    char* str;
    Cli_flush_input();
    if ((str = Cli_prompt("Gullkorn")) != NULL)
      Rbuf_put(gems, str);
    Cli_menu_prompt();
}

char Cli_choose(rbuf_t *gems)
{
    char choice;
    switch(choice = getc(stdin))
    {
      case 'g':   Cli_print_gem(gems);  break;
      case 'b':   Cli_contrib_gem(gems);break;
      case '\n':  Cli_menu_prompt();    break;
      case  -1:   choice = 'a';
      case 'a':   break;
      default:
        printf("Ukjent kommando: %c\n", choice);
    }
    return choice;
}

void Cli_loop()
{
    DLOG("Loading gems..\n");
    rbuf_t *gems;
    gems = Gem_read_or_create(GEMLIST_PATH);

    DLOG("Starting CLI loop..\n");
    Cli_menu_prompt();
    while(Cli_choose(gems) != 'a');
    DLOG("Leaving CLI loop..\n");

    if (Gem_write(gems, GEMLIST_PATH) == -1)
      perror("Error writing gems to disk");
    Rbuf_free(gems);
    /* The strings in the ring buffer are not
     * deallocated, and therefore detected
     * as a leak. */
}

int main(void)
{
    DLOG("DEBUG is set.\n");
    Cli_loop();
    DLOG("Peacefully shut down.\n");
    return 0;
}

