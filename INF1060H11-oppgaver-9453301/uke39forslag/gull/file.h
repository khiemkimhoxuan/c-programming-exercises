
#ifndef GULL_FILE_H
#define GULL_FILE_H

#include <stdio.h>

char *File_get_line(FILE *, int);
int   File_path_exists(char *);

#endif
