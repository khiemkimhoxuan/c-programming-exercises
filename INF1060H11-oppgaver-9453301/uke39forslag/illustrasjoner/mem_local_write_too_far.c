#include <stdio.h>
#include <stdlib.h>

/* One example of a memory errror that
 * is not detected by valgrind.
 *
 * The error could have different consequences
 * depending on the local machine.
 *
 * When i tried on the ifi servers, x and y were
 * overwritten during the final iterations of the
 * for loop.
 */

int main(int argc, char** argv){
  int i;
  int x=0, y=0,z=0;
  int a[10];
  for (i = 0; i < 15; i++)
    a[i] = i;
    
  printf("x is %d\n", x);
  printf("y is %d\n", y);
  printf("z is %d\n", z);
  return 0;
}

