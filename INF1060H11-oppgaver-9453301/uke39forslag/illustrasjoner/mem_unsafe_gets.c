
#include <stdio.h>
#include <stdlib.h>

/* The gets function should never be used,
 * because it keeps reading until a line break
 * is encountered. If there are more
 * characters than the size of the buffer,
 * it will cause a memory error.
 *
 * As an alternative, see getline.
 *
 */

int main(int argc, char** argv){
  char *str = malloc(10);
  gets(str);
  printf("%s\n",str);
  return 0;
}

