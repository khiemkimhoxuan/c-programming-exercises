

#include <stdio.h>
#include <stdlib.h>


int main(int argc, const char *argv[])
{

    while (1) {

        if (vfork() == 0) {
            execv(argv[1], &argv[1]);
        }
        sleep(1);
    }

    return 0;
}
