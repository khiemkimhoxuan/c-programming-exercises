
#ifndef GULL_DEBUG_H
#define GULL_DEBUG_H

#include <stdio.h>

#ifdef DEBUG
    #define DLOG(args...) fprintf(stderr, "~> "); fprintf(stderr, args)
#else
    #define DLOG(args...)
#endif

#endif
