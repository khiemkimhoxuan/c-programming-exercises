

#### Suggested solutions, week 44: Networks


#### 1

www.ifi.uio.no is a nickname for kili.ifi.uio.no. This means that computers can have several names.  login.ifi.uio.no returns several addresses. This means that a name can belong to more than one address (even to more than one computer).


#### 2

You find out whether a computer has an official name. You find out how much time a packet has needed for the trip to the router.  You will often see `* * *` instead of a hostname. This means that the router has not answered. This may be a firewall that refuses to send a reasonable answer, or a router that refuses to answer. The first case is worse because traceroute will never stop trying to find the next router on the path, and the firewall will always delete the query packets.


#### 3

Many tasks have to be accomplished to communicate over the network. 
These tasks should be modularized to make implementation and understanding easier. Modules can be upgraded or replaced when necessary, without affecting other modules.  Layering is a simple variation of modularization that provides a clear structure. Each module is at exactly one layer, and offers its service only to modules at the next upper layer, rather than generally to other modules.


#### 4

**Physical layer:** Carry bits.

**Link layer:** Transport messages between two machines that are directly connected with each other. 
	
**Network layer:** Address end systems that are connected to the network, and find the way of messages through the network to their destination. 

**Transport layer:** Address processes/applications on end systems and make sure that applications can exchange data. 

**Application layer:** Implements special protocols such as FTP, HTTP, SMTP. Usually own programs are implemented on this layer.


#### 5

**Session layer:** Add a concept of sessions. Applications on end systems set up a session (connect to each other) and know that they communicate with each other while the session exists. Tear down the session.  Determine when something has gone wrong and fix it. In the worst case, terminate the session and inform the application.
	
**Presentation layer:** Translate data that is exchanged, so that it can be	used on different architectures. E.g. big endian into little endian codes.
	
In the Internet, applications have to implement these services if they need them. Some Internet protocol achieve these functions without being called session layer or presentation layer protocols.


#### 6

In circuit switching, a communication is set up before messages are exchanged. In packet switching, each message is sent out individually and must find its path through the network.

You can reserve resources in circuit switched networks, while that is nearly impossible in packet switching ones.


#### 7

The units of the layer must exchange information to provide their service, e.g. to address a target application or to address a target computer. A layer offers its services to the upper layers but must not interfere with the messages that the upper layers exchange. Therefore, they must extend the messages by adding information somewhere.

Adding it in front is a typical choice (but not the only one), because messages are sent from beginning to end over the physical medium. If the header is at the front, the receiver can start processing before all of the message has arrived.

