


### Løsningsforslag uke 40


#### 1:

For de fleste systemene med virtuelt minne er hukommelsen delt opp i
sider av fast størrelse. Hvilken som helst ramme kan få plass i
hvilken som helst side, og dermed er den fragmenteringen som oppstår
kun *intern* fragmentering - dvs. at en ramme ikke helt er stor nok
til å fylle opp en side.

Siden de fleste prosesser trenger minst tre rammer (en til data, en til
program og en til prosesskontroll) vil altså en prosess i verste fall
legge beslag på tre rammer som den bare trenger litt av.  Dette
problemet øker med økende rammestørrelse, men er uansett lite i
systemer med virtuelt minne.


#### 2:

1. Hensikten er å gjøre tildelingen av prosessortid mer rettferdig ved at de prosessene som har mye igjen av sitt tildelte tidskvantum settes inn lenger fram i proessorkøen enn de som nesten har brukt det opp. Prosessene settes inn i køen noenlunde i henhold til forbrukt tid.

2. Fordelen er at man får en mer rettferdig tildeling av prosessoren.  Ulempen er at prosessoren må bruke tid på å regne ut hvor i køen prosessen skal plasseres hver gang prosessen gir fra seg prosessoren eller interruptes.



#### 3:

Bruk av virtuelt minne medfører at sidene til en prosess kan swappes ut for å spare minne, når de ikke er i bruk. Det kan altså gå med tid til å swappe inn eller ut sider under normal operasjon av et program. Det er ikke akseptabelt for et sanntids-system, der man ikke må bruke mer tid enn ventet. OS-et kan eventuelt swappe ut de prosessene som ikke behøver å være i sanntid, hvis det lar sanntidsprosessene være i minnet.


#### 4:

1)
    
    0172 Minnet fylles initielt opp. 4 page faults.
    1723 Page fault; 3 skal inn.
    1723 Side 2, 7 og 1 brukes i rekkefÃ¸lge.
    7230 Page fault; 0 skal inn.
    7230 3 brukes.
    ==> 6

2
    0172 Minnet fylles iniyielt opp. - 4 page faults
    1723 Page fault; 3 skal inn.
    1723 Side 2, 7 og 1 brukes i rekkefÃ¸lge.
    1720 Page fault; 0 skal inn 3 gÃ¥r ut (ligget lengst).
    1703 Page fault; 3 skal inn 2 gÃ¥r ut (ligget lengst).
    ==> 7

3
    Ved bruk av LRU må systemet hele tiden holde orden på hvilke sider 
    som har ligget lengst i minne. Med et stort antall sider, vil 
    dette innebære mye bokføring, og man vil fortsatt kunne få
    situasjoner som den i oppgaven over, der en side swappes ut for 
    deretter å skulle brukes igjen umiddelbart.


#### 5:

En *page fault* hver `k` instruksjon gir ekstra overhead på `n/k 
nanosekunder` til gjennomsnittet. Så en gjennomsnittlig instruksjon 
tar:

    10 + n/k nanosekunder


#### 6:

20 bits brukes til *virtual page* nummer, så vi har 12 igjen til 
offset. Dette gir oss en 4KB *page*. 20 bits til *vitual page* nummer 
gir `2^20` pages.

Det har ingenting å si om vi bruker 11 bit på første nivå og 9 på andre 
nivå.


#### 7:

Med 8KB pages og 48-bit virituelt addresseområde, blir antall 
virituelle pages:

    2^48 / 2^13 = 2^35


