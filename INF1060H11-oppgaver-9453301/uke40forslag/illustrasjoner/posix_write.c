
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> /* posix api's */
#include <fcntl.h>  /* posix file control */

#define NUM_FILE "./numbers.dat"

int main(int argc, const char *argv[])
{
    int nums[4];
    int fd;
    size_t bytes_written;
    int close_result;

    nums[0] = 3;
    nums[1] = 5;
    nums[2] = 8;
    nums[3] = 13;

    fd = open(NUM_FILE, O_WRONLY | O_CREAT);

    if (fd == -1) {
        perror("Failed to open file");
        return EXIT_FAILURE;
    }

    bytes_written = write(fd, nums, sizeof(int) * 4);
    close_result = close(fd);

    printf("Wrote %zu bytes. close(fd) %s.\n", bytes_written,
        close_result == 0 ? "succeeded" : "failed");

    return EXIT_SUCCESS;
}

