#include <stdio.h>
#include <stdlib.h>

typedef struct {
    int x;
    int y;
} point_t;

#define POINTS_FILE "./points.dat"


int main(int argc, const char *argv[])
{
    point_t p[4];
    FILE *fd;
    size_t bytes_read;
    int close_result;

    fd = fopen(POINTS_FILE, "r");

    if (fd == NULL) {
        perror("Failed to open file");
        return EXIT_FAILURE;
    }

    bytes_read = fread(&p, sizeof(point_t), 4, fd);
    close_result  = fclose(fd);

    printf("Read %zu items. fclose(fd) %s.\n", bytes_read,
        close_result == 0 ? "succeeded" : "failed");

    printf("%d, %d\n", p[0].x, p[0].y);
    printf("%d, %d\n", p[1].x, p[1].y);
    printf("%d, %d\n", p[2].x, p[2].y);
    printf("%d, %d\n", p[3].x, p[3].y);

    return EXIT_SUCCESS;
}

