

#include <stdio.h>
#include <stdlib.h>


typedef struct {
    char *name;
    int age;
} person_t;


#define PEOPLE_FILE "./people.dat"


/*  fopen() accepts these flags:
 *
 *  Flag:   Open for:               Start at:
 *
 *  r       read                    beginning
 *  w       overwrite or create     beginning
 *  a       append or create        end
 *  r+      read and write          beginning
 *  w+      read and overwrite      beginning
 *  a+      read and append         end
 */


int main(int argc, const char *argv[])
{
    FILE *fd;
    int chars_printed = 0;
    int close_result;
    person_t p[2];

    p[0].name = "May";
    p[0].age  = 25;
    p[1].name = "Leif";
    p[1].age  = 22;

    fd = fopen(PEOPLE_FILE, "w");

    if (fd == NULL) {
        perror("Failed to open file");
        return EXIT_FAILURE;
    }

    chars_printed += fprintf(fd, "%d:%s\n", p[0].age, p[0].name);
    chars_printed += fprintf(fd, "%d:%s\n", p[1].age, p[1].name);
    
    close_result = fclose(fd);

    printf("Wrote %d chars. fclose(fd) %s.\n", chars_printed,
        close_result == 0 ? "succeeded" : "failed");

    return EXIT_SUCCESS;
}



