
#include <stdio.h>
#include <stdlib.h>


typedef struct {
    char *name;
    int age;
} person_t;


#define PEOPLE_FILE "./people.dat"


/*  fopen() accepts these flags:
 *
 *  Flag:   Open for:               Start at:
 *
 *  r       read                    beginning
 *  w       overwrite or create     beginning
 *  a       append or create        end
 *  r+      read and write          beginning
 *  w+      read and overwrite      beginning
 *  a+      read and append         end
 */


int main(int argc, const char *argv[])
{
    FILE *fd;
    int close_result;
    int items_matched = 0;
    person_t a, b;

    fd = fopen(PEOPLE_FILE, "r");

    if (fd == NULL) {
        perror("Failed to open file");
        return EXIT_FAILURE;
    }

    a.name = malloc(100);
    b.name = malloc(100);

    items_matched += fscanf(fd, "%d:%s\n", &a.age, a.name);
    items_matched += fscanf(fd, "%d:%s\n", &b.age, b.name);

    close_result = fclose(fd);

    printf("Matched %d items. fclose(fd) %s.\n", items_matched,
        close_result == 0 ? "succeeded" : "failed");

    printf("%s (%d)\n", a.name, a.age);
    printf("%s (%d)\n", b.name, b.age);

    return EXIT_SUCCESS;
}



