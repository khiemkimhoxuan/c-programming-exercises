#include <stdio.h>
#include <stdlib.h>

typedef struct {
    int x;
    int y;
} point_t;

#define POINTS_FILE "./points.dat"


int main(int argc, const char *argv[])
{
    point_t p[4];
    FILE *fd;
    size_t bytes_written;
    int close_result;

    p[0].x = 0;
    p[0].y = 0;
    p[1].x = 0;
    p[1].y = -100;
    p[2].x = 100;
    p[2].y = -100;
    p[3].x = 100;
    p[3].y = 0;

    fd = fopen(POINTS_FILE, "w");

    if (fd == NULL) {
        perror("Failed to open file");
        return EXIT_FAILURE;
    }

    bytes_written = fwrite(&p, sizeof(point_t), 4, fd);
    close_result  = fclose(fd);

    printf("Wrote %zu items. fclose(fd) %s.\n", bytes_written,
        close_result == 0 ? "succeeded" : "failed");

    return EXIT_SUCCESS;
}

