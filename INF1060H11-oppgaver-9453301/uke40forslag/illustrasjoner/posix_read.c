
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> /* posix api's */
#include <fcntl.h>  /* posix file control */

#define NUM_FILE "./numbers.dat"

int main(int argc, const char *argv[])
{
    int nums[4];
    int fd;
    size_t bytes_read;
    int close_result;

    fd = open(NUM_FILE, O_RDONLY);

    if (fd == -1) {
        perror("Failed to open file");
        return EXIT_FAILURE;
    }

    bytes_read = read(fd, nums, sizeof(int) * 4);
    close_result = close(fd);

    printf("Read %zu bytes. close(fd) %s.\n", bytes_read,
        close_result == 0 ? "succeeded" : "failed");

    printf("%d, %d, %d, %d\n", nums[0], nums[1], nums[2], nums[3]);

    return EXIT_SUCCESS;
}

