#include <stdio.h>
#include <string.h>
#include <assert.h>

int main(int argc, char** argv) {
    // skriv ut bokstav for bokstav
    
    // metode en:
    int i, k;
    for (i=0; i<argc; i++) {
        for (k=0; k<strlen(argv[i]); k++) {
            printf("argv[%d][%d] = '%c'\n", i, k, argv[i][k]);
        }
        printf("argv[%d][%d] = '\\0'\n", i, k);

        printf("\n");
    }
    // med pointere...
    printf("med pointere: \n");
    for (i=0; i<argc; i++) {
        char *s = argv[i];

        for (; *s; s++) {
            printf("argv[%d][%d] = '%c'\n", i, (int)(s-argv[i]), *s);
        }
        printf("argv[%d][%d] = '\\0'\n", i, (int) (s - argv[i]));
        printf("\n");
    }

    printf("\n");

    // skriv ut hele argumentet i en sleng:
    for (i=0; i<argc; i++) {
        char *s = argv[i];
        printf("argv[%d] = '%s' == '%s' == '%s'\n", i, s, argv[i], &argv[i][0]);
        assert(s == argv[i]);
        assert(argv[i] == &argv[i][0]);
    }



    return 0;
}
