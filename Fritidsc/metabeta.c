#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <time.h>


void randomSleep(int time) {
    srand(getpid());
    int randomize = rand() % time;
    printf("process is sleeping [%d]",getpid());
    sleep(randomize);
}

 sumProcess(int a, int b) {
    int result = 0;
    int i;
    if(fork()) {
        return;
    }
    printf("Child process is is running [%d] \n", getpid());
    randomSleep(5);
    for(i = a; i<b; i++) {|
        result += i;
    }

    printf("result:  %d \n",result);
    exit(result);
}



int main(void) {
    clock_t start = clock();
    const time_t timer = time(NULL);
    printf("Time date: %s \n",ctime(&timer));
    printf("Time used %f\n", ((double)(clock()) - start));
    int status,pid,i,val;
    for(i = 0; i<3; i++) {
        pid = wait(&status);
        
    }
}

