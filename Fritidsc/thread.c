#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <pthread.h>

struct data {
    int start;
    int end;
    int result;
} ;

void* sumfn(void* arg) {
    struct data* p = (struct data*) arg;
    int i, s = 0;
    for (i=p->start; i<p->end; i++)
        s+= i;

    p->result = s;
    return 0;
}

int main(int argc, char** argv) {

    int num_threads = 3;
    int step = 999;
    int curr = 1;
    int i;

    pthread_t threads[num_threads];
    struct data dat[num_threads];

    for (i=0; i<num_threads; i++) {
        dat[i].start = curr;
        dat[i].end = curr+step;
        curr = curr+step+1;
    }

    for (i=0; i<num_threads; i++) {
        printf("creating thread %d\n", i);
        int res = pthread_create(&threads[i], NULL, sumfn, (void*)&dat[i]);
        assert(res == 0);
    }

    for (i=0; i<num_threads; i++) {
        printf("joining thread %d\n", i);
        int res = pthread_join(threads[i], NULL);
        assert(res == 0);
    }

    int sum = 0;
    for (i=0; i<num_threads; i++) {
        sum += dat[i].result;
    }
    printf("The sum was %d\n", sum);
    //data[0]

    return 0;
}

