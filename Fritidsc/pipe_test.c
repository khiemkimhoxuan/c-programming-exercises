#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>



void child(int r, int w){
	char msg[20];
	while(1){
		read(r, msg, 20);
		fprintf(stderr, "child received: %s\n", msg);

		if(strncmp(msg, "Wake up!", 8) == 0){
			char *reply = "One minute!";
			fprintf(stderr, "child sending: %s\n", reply);
			write(w, reply, strlen(reply));

			// Give parent time to receive response. If we do not do this,
			// we might read our own response when the while-loop restarts.
			sleep(1);
		}
		else if(strncmp(msg, "Good night", 10) == 0){
			fprintf(stderr, "child: exit\n");
			exit(0);
		}
	}
}


void parent(int r, int w){

	// Send "Hello" to child and give it 1 second to receive.
	// we assume we will not get a response.
	write(w, "Hello", 6); // note that we send 5 characters + the \0 byte..
	sleep(1);

	// Send "Wake up!" to child and give it 1 second to respond
	write(w, "Wake up!", 9);
	sleep(1);

	// Read response from child
	char reply[15];
	read(r, reply, 15);
	fprintf(stderr, "Parent received: %s\n", reply);
	sleep(1);

	// Send "Good night" to child and exit.
	write(w, "Good night", 11);
	exit(0);
}


int main(){
	int fd[2];
	if(pipe(fd) != 0){
		perror("pipe");
		return 1;
	}

	if(fork() == 0){
		child(fd[0], fd[1]);
	}
	else{
		parent(fd[0], fd[1]);
	}

	return 0;
}
