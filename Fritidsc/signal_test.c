#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>



/** Interrupt handler. Stops the program. */
void inthandler(int signum){
	fprintf(stderr, "goodbye\n");
	exit(1);
}

/** Pause the process. It will pause until another signal
 * is received. */
void pausehandler(int signum){
	fprintf(stderr, "pause\n");
	pause();
}

/** Continue a paused program. */
void continuehandler(int signum){
	fprintf(stderr, "continue\n");
}


void debug(msg){
	printf(msg);
}

int main() {
	int pid = getpid();
	fprintf(stderr, "Starting process with PID: %d\n", pid);

	// Show help
	fprintf(stderr, "Use 'kill -s INT %d' to stop the program.\n", pid);
	fprintf(stderr, "Use 'kill -s TSTP %d' to pause the program.\n", pid);
	fprintf(stderr, "Use 'kill -s CONT %d' to continue the program.\n", pid);
	// note that the kill() function could be used instead of the kill program.

	// Setup signal handlers
	signal(SIGINT, inthandler);
	signal(SIGTSTP, pausehandler); // 
	signal(SIGCONT, continuehandler);

	// Wait for signals
	while(1){
		fprintf(stderr, "*");
		sleep(1);
	}

	return 0;
}
