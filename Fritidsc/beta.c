
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>

/* array size */
#define MAXARRAY 999999

#define CPU_TIME (getrusage(RUSAGE_SELF,&ruse), ruse.ru_utime.tv_sec + \
  ruse.ru_stime.tv_sec + 1e-6 * \
  (ruse.ru_utime.tv_usec + ruse.ru_stime.tv_usec))

/* get resource limits and usage */
extern int getrusage();
struct rusage ruse;

/* compare function for qsort() */
int cmp(const void *a, const void *b) {
 const int *x = (const int *)a;
 const int *y = (const int *)b;
 return(*x > *y) - (*x < *y);
}

int main(void) {
 int array[MAXARRAY] = {0};
 double first, second;
 time_t start, end;
 int i = 0;

 /* start */
 time(&start);
 first = CPU_TIME;

 /* produce some cpu cycles... */
 /* load some random values */
 for(i = 0; i < MAXARRAY; i++)
  array[i] = rand() % 100;
 /* qsort the array */
 qsort(array, MAXARRAY, sizeof(int), cmp); 

 /* end */
 second = CPU_TIME;
 time(&end);

 printf("cpu  : %.2f secs\n", second - first); 
 printf("user : %d secs\n", (int)(end - start));
}
