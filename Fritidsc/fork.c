#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <time.h>

int sum(int start, int slutt) {
    int result = 0;
    int i = 0;
    for (i = start; i < slutt; i++) {
        result += i;
    }
    return result;
}

void sleepRandom(int i) {
    srand(getpid());
    int w = rand() % i;
    fprintf(stderr, "Child process [%d] is now sleeping \n", getpid());
    sleep(w);
}

int readFileSumFork(char *file) {
    FILE *f;

    f = fopen(file, "r");
    if (f == NULL) {
        fprintf(stderr, "error,[%d] process could not read \n", getpid());
        exit(-1);
    }
    char buf[15];
    fread(buf, 1, 15, f);

    if (ferror(f) != 0) {
        fprintf(stderr, "error [%d] another process failed \n", getpid());
        exit(-2);
    }

    fclose(f);
    return (int) atol(buf);
}

void readFile(int start, int slutt, char *file) {
    FILE *f;
    if (fork() == 0) {
        printf("[%d] process starting \n", getpid());
        int s = sum(start, slutt);
        sleepRandom(5);
        f = fopen(file, "w");
        if(f == NULL) {
            fprintf(stderr,"Error, process [%d] failed to write \n",getpid());
            exit(1);
        }
        if(fprintf(f,"%d",s) < 0) {
            fprintf(stderr,"Error, process [%d] failed to write again \n",getpid());
            exit(2);
        }

        exit(0);
    }else {
        printf("This is a parent process %d \n",getpid());
        
    }

}

int main() {
    readFile(1,1000,"sum1.txt");
    readFile(1001,2000,"sum2.txt");
    readFile(2001,3000,"sum3.txt");

    int status, i,childpid;
    int exit;
    const time_t timer = time(NULL);
    for(i = 0; i< 3; i++) {
        childpid = wait(&status);
        fprintf(stderr,"child process[%d] terminated with status %d \n",childpid,status);

        if(status != 0) {
            exit++;
        }
    }
    int sum = readFileSumFork("sum1.txt");
    sum += readFileSumFork("sum2.txt");
    sum += readFileSumFork("sum3.txt");

    printf("Sum: %d \n",sum);
    printf("Todays time is %s",ctime(&timer));
    return exit;
}