#include <stdio.h>
#include <time.h>
int fib(int n) {
    if (n <= 1) {
        return 1;
    } else {
        return fib(n - 1) + fib(n - 2);
    }
}

int main(void) {
    int type;
    int i;
    clock_t start = clock();
    printf("Type in:  \n");
    scanf("%d", &type);
    for (i = 0; i < type; i++) {
        printf("Fibonacci: %d \n", fib(i));
        printf("Time elapsed: %.3f\n", ((double)clock() - start) / CLOCKS_PER_SEC);

    }



}