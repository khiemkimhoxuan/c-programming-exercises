#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>

void sleepRandom(int m) {
    srand(getpid());
    int w = rand() % m+1;
    fprintf(stderr,"process[%d] sleeps for %d seconds... \n",getpid(),w);
    sleep(w);
}

void sum(int start, int slutt) {
    int result = 0;
    int i;
    if(fork()) {
        return;
    }
    printf("[%d] child process is running... \n",getpid());
    sleepRandom(5);
    for(i = start; i<slutt; i++) {
        result += i;
    }

    printf("result %d \n",result);
    exit(result);
}

int main() {
    int status;
    int i;
    int val = 0;
    sum(1,1000);
    sum(1001,2000);
    sum(2001,3000);
    for(i = 0; i<3; i++) {
        int pid = wait(&status);
        int result = WEXITSTATUS(status);
        val += result;
        printf("val %x , %x \n",val,status);
    }
    printf("The result was: %d \n",val);

    const time_t timer = time(NULL);
    printf("Time used on the project %s \n", ctime(&timer));
    return 0;
}

