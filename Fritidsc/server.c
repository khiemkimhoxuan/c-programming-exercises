#include <netinet/in.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char ** argv) {
    struct sockaddr_in clientaddr;
    socklen_t clientaddrlen;
    int client_sock,sock;

    char buff[200];

    if(argc < 2) {
        printf("usage: %s <port-number> \n",argv[0]);
        exit(1);
    }
    char * port_str = argv[1];
    int rv;

    struct addrinfo hints, *servinfo;
    memset(&hints,0,sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    rv = getaddrinfo(NULL,port_str,&hints,&servinfo);

    if(rv != 0) {
        printf("getaddrinfo: %s \n",gai_strerror(rv));
       exit(1);
    }

    sock = socket(servinfo->ai_family,servinfo->ai_socktype,servinfo->ai_protocol);
    if(sock == -1) {
        perror("socket");
        exit(1);
    }

    int yes = 1;
    if(setsockopt(sock,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(int)) == -1 ) {
        perror("setsockopt");
        exit(1);
    }

    if(bind(sock,servinfo->ai_addr, servinfo->ai_addrlen) == -1) {
        close(sock);
        perror("bind");
        exit(1);
    }

    rv = listen(sock,SOMAXCONN);
    if(rv == -1) {
        perror("listen");
        close(sock);
        exit(1);
    }

    printf("waiting for connection ---\n");
    client_sock = accept(sock,(struct sockaddr *)&clientaddr,&clientaddrlen);
    if(client_sock == -1) {
        perror("accept");
        close(sock);
        exit(1);
    }
    rv = read(client_sock,buff,sizeof buff);
    if(rv == -1) {
        perror("read");
        close(sock);
        exit(1);
    }

    if(rv > 0) {
        printf("fikk %d bytes \n",rv);
        buff[rv] = 0;
        printf("fikk: ");
        printf("%s",buff);
        printf("\n");
    }

    close(sock);
    close(client_sock);

    return 0;
}
