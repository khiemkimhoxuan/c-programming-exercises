#include <stdio.h>
#include <time.h>
#include <assert.h>

void printbin(int x, int numbits) {
    int i = 0;
    numbits--;
    assert(numbits >=0);
    for(i = numbits; i >= 0; i--) {
        int bit = (x >> i) &1;
        printf("%c",bit == 0 ? '0':'1');
    }
    printf("\n");
}

void bit_eat(int *dest, int inbits, int numbits) {
    numbits = 2;
    
}


int main()
{
    const time_t timer = time(NULL);
    printf("time is %s\n", ctime(&timer));

	unsigned char p = 5;     // 0000 0101
	unsigned char k = 3;     // 0000 0011

	unsigned char a = p & k; // 0000 0001  (1)
	unsigned char o = p | k; // 0000 0111  (7)
	unsigned char x = p ^ k; // 0000 0110  (6)
	unsigned char n = ~p;    // 1111 1010  (250)
	assert(a == 1);
	assert(o == 7);
	assert(x == 6);
	assert(n == 250);
        printf("a is %x in hexadecimal \n",a);
        printf("o is %x in hexadecimal \n",o);
        printf("x is %x in hexadecimal \n",x);
        printf("n is %x in hexadecimal \n",n);
        printbin(a,7);
        printbin(o,7);
        printbin(x,7);
        printbin(n,7);


    return 0;
}
