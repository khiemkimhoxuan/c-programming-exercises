#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <assert.h>


int main(int argc, char **argv) {
    if(argc < 3) {
        fprintf(stderr,"usage: %s <hostname> <port-number> \n",argv[0]);
        exit(1);
    }

    int port = atoi(argv[2]);
    char * port_str = argv[2];
    char *hostname = argv[3];
    int sock;

    printf("Connecting to %s : %d \n",hostname,port);

    struct addrinfo hints, *res;
    memset(&hints,0,sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;

    int status = getaddrinfo(hostname,port_str,&hints,&res);

    if(status != 0) {
        printf("getaddrinfo: %s \n",gai_strerror(status));
        exit(1);
    }

    sock = socket(res->ai_family,res->ai_socktype,res->ai_addrlen);
    if(sock == -1) {
        perror("socket");
        exit(1);
    }

    if(connect(sock,res->ai_addr,res->ai_addrlen) == -1) {
        perror("connect");
        exit(1);
    }
    printf("Connected \n");

    char buff[200];
    printf(">");
    fflush(stdout);
    fgets(buff,sizeof buff,stdin);

    char *pos = strrchr(buff,'\n');
    *pos = 0;
    printf("fik %s, lengde %zu \n",buff,strlen(buff));


    int num_bytes = sizeof(buff[0]) * (strlen(buff) +1);
    int retval = send(sock,buff,num_bytes,0);
    if(retval == -1) {
        perror("send");
        exit(1);
    }
    printf("sent %d of %d bytes \n",retval,num_bytes);

    freeaddrinfo(res);
    if(-1 == close(sock)) {
        perror("close");
    }
    return 0;
}