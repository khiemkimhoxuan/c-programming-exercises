/*
 * valgrind ./1
 * gir
 *
 *  Invalid read of size 1
==25876==    at 0x4A066D4: strlen (mc_replace_strmem.c:246)
==25876==    by 0x3C80846B68: vfprintf (in /lib64/libc-2.5.so)
==25876==    by 0x3C8084D3F9: printf (in /lib64/libc-2.5.so)
==25876==    by 0x40064C: main (1.c:20)
==25876==  Address 0x4C3F037 is 0 bytes after a block of size 7 alloc'd
==25876==    at 0x4A05809: malloc (vg_replace_malloc.c:149)
==25876==    by 0x4005AE: kopi_feil (1.c:8)
==25876==    by 0x400635: main (1.c:19)
 *
 * Altså:
 *
 * printf prøver å lese en byte som ikke er allokert.
 * Dette er fordi vi bare allokerer strlen av p.
 * Dermed går vi 'glipp av' 0-en på slutten av teksten.
 * 
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

char* kopi_feil(char* p) {
    int i;
    char* ny = malloc(strlen(p)); // <<--- feil!
    // burde være:
    // char* ny = malloc(strlen(p)+1);

    for (i=0; i<strlen(p); i++) {
        ny[i] = toupper(p[i]);
    }

    // må også skrive null byte
    // ny[i] = '\0';

    return ny;
}

int main(int argc, char** argv) {

    char* test = kopi_feil("heisann");
    printf("Fikk '%s'\n", test);

    free(test);
    return 0;
}


