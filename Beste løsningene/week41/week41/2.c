/*
 * valgrind ./2
 * gir:
 *
 *   Conditional jump or move depends on uninitialised value(s)
==26524==    at 0x400558: main 
 *
 * Dette skjer typisk hvis man har en
 *
 * if (min_struct->verdi == 123) {
 * }
 *
 * og man ikke har satt verdi noen gang.
 * Typisk glemmer man dette etter å ha tatt en malloc.
 * Det *kan* også skje med en allokert variabel på stacken. (lokal variabel)
 *
 * Konklusjon: Alltid sett verdier før du sammenligner de med noe.
 *
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

struct demo {
    int a;
    int b;
};

int main(int argc, char** argv) {

    struct demo* x = malloc(sizeof(struct demo));
    x->a = 123;

    if (x->b==0) {
        printf("b er 0\n");
    }
    else {
        printf("b er ikke 0\n");
    }

    free(x);

    return 0;
}


