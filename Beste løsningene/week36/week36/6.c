#include <assert.h>


/** Check if *c* is a hex number.
 * @return 1 of true and 0 if false.
 */
int ishex(unsigned char c){
	return ((c>='0' && c<='9') ||
			(c>='a' && c<='f') ||
			(c>='A' && c<='F'));
}


/** Convert the hex number *c* to decimal.
 * @return -1 if *c* is not a hex number.
 */
int hexval(unsigned char c){
	if(ishex(c)){
		if(c >= 'a')
			return c - 'a' + 10;
		else if(c >= 'A')
			return c - 'A' + 10;
		else
			return c - '0';
	}
	else
		return -1;
}


int main(){

	// Some tests.
	// See *man 3 assert* for information about *assert*.

	assert(ishex('0') == 1);
	assert(ishex('a') == 1);
	assert(ishex('F') == 1);
	assert(ishex('d') == 1);
	assert(ishex('T') == 0);

	assert(hexval('0') == 0);
	assert(hexval('9') == 9);
	assert(hexval('c') == 12);
	assert(hexval('D') == 13);
	assert(hexval('T') == -1);

	return 0;
}
