/* This is a simple solution to the problem. It only handles *cstrings*, *int*
 * and *float*. Making it understand the difference between *float* and
 * *double*, *short* and *int* and so on is a challenge I leave up to you.
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>


/** Check if a string only contains an integer.
 * @param s A null terminated character array.
 * @return 1 if true and 0 if false.
 * */
int isint(char *s){
	int i;
	for(i=0; i<strlen(s); i++){
		if(isdigit(s[i]))
			continue;
		else
			return 0;
	}
	return 1;
}


/** Check if a string only contains a floating point number.
 * @param s A null terminated character array.
 * @return 1 if true and 0 if false.
 */
int isfloat(char *s){
	int i;
	int commas = 0;
	for(i=0; i<strlen(s); i++){
		if(isdigit(s[i]))
			;
		else if(s[i] == '.'){
			commas += 1;
		}
		else
			return 0;
	}
	return (commas == 1); // we only count it as a float if it has one '.'
}


int main(int argc, char **argv){
	if(argc != 2){
		fprintf(stderr, "Usage: %s <message>\n", argv[0]);
		return 1; // Return values from main other than 0 signals an error.
	}

	char *msg = argv[1];

	if(isfloat(msg)){
		float f = atof(msg);
		printf("Float input: %f\n", f);
	}
	else if(isint(msg)){
		int i = atoi(msg);
		printf("Integer input: %d\n", i);
	}
	else
		printf("Input: %s\n", msg);

	return 0;
}
