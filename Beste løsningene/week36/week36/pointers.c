#include <stdio.h>
#include <assert.h>

void pointer_demo_1() {
    int a;
    int* p;

    a = 5;
    p = &a;     // p peker nå til a
    printf("a=%d, &a=%p, p=%p \n", a, &a, p);

    assert(p == &a);

    *p += 10;   // skriver til a ved hjelp av p
    printf("a=%d \n", a);
}

void pointer_demo_2() {
    int a=1;
    int b=2;

    int* p;

    p = &a;     // p peker til a
    //p = malloc(sizeof(int));    // skriver til a ved hjelp av p
    *p = 10;

    p = &b;     // p peker til b
    *p = 20;    // skriver til b ved hjelp av p

    printf("\na=%d, b=%d\n", a,b);
}

void modify(int* x) {
    *x = *x + 100;
}

void pointer_demo_3() {
    int a=1;
    modify(&a);     // vi gir adressen til a, dvs. en peker til a, til funksjonen
                    // slik at den kan 
    printf("\na=%d\n", a);
}

void pointer_demo_4() {
    int rekke[] = {1,2,3};

    int* forste = &rekke[0];
    int* andre = &rekke[1];
    int* tredje = &rekke[2];

    int *peker = rekke;

    assert(peker == forste);
    assert(forste == rekke);


    printf("\nPointer_demo_4():\n");

    printf("Forste: %d, Andre: %d, Tredje: %d\n", *forste, *andre, *tredje);

    peker[0] = 123;
    peker++;            // peker = &rekke[1]

    *peker = 456;       // rekke[1] = 456
    peker++;            // peker = &rekke[2]

    *peker = 789;       // rekke[2] = 789
    printf("Forste: %d, Andre: %d, Tredje: %d\n", *forste, *andre, *tredje);

}

int main(int argc, char** argv) {

    pointer_demo_1();
    pointer_demo_2();
    pointer_demo_3();
    pointer_demo_4();

    return 0;
}

