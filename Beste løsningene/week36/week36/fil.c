#include <stdio.h>
#include <assert.h>

void fil_demo_1() {
    char filnavn[] = "./hei.txt";
    FILE* fd = fopen(filnavn, "r");
    assert(fd != NULL);

    int c;

    while(1) {
        c = getc(fd);
        if (c==EOF) { break; }

        printf("fikk '%c'\n", c);
    }

    /*
    while (EOF != (c = getc(fd))) {
        printf("fikk '%c'\n", c);
    }  
    */

    assert(0 == fclose(fd));
}

void fil_demo_2() {
    char filnavn[] = "./input.txt";
    char tmp[128];
    int x[10];
    FILE* fd = fopen(filnavn, "r");
    assert(fd != NULL);


    int a,b;
    int results;

    while (1) {

        results = fscanf(fd, "%d %d", &a, &b);
        if (results!=2) {
            break;
        }
        printf("fikk %d, %d\n", a, b);
    }

    assert(NULL != fgets(tmp, sizeof(tmp), fd));
    printf("fikk tekst '%s'\n", tmp);

    assert(0 == fclose(fd));
}

struct punkt {
    int x, y;
};

void struct_demo() {
    struct punkt p; // = malloc(sizeof(struct punkt));
    p.x = 10;
    p.y = 20;

    struct punkt* aa;
    aa = &p;

    aa->x = 100;
    aa->y = 200;

    (*aa).x = 100;
    (*aa).y = 200;


}

int main(int argc, char* *argv) {

    fil_demo_1();
    printf("\n\n");
    fil_demo_2();

    return 0;
}
