#include <assert.h>
#include <string.h>

#define STREQ(s1,s2) (strcmp((s1),(s2))==0)

#define dobbel(A) (A+A) 

int main(){
	// Some tests.
	// See *man 3 assert* for information about *assert*.
    //int i = 0;

    //dobbel(++i); // dette gir ++i + ++i

	assert(STREQ("Hello", "Hello") == 1);
	assert(STREQ("Hello", "x") == 0);
	return 0;
}
