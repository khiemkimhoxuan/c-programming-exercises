#include <stdio.h>

int main(){
	short a, b, sum;

	a = 20000;  b = 20000;  sum = a+b;

	/* - If *a* is negative and *b* is positive or visa versa, no
	 *   overflow can occur.
	 * - If they are signed the same way and the result is not signed
	 *   that way, overflow has occurred. */
	if(a<0 == b<0  &&  a<0 != sum<0){
		fprintf(stderr, "** error: Number overflow\n");
		return 1;
	}
	printf("%d + %d = %d\n", a, b, sum);
	return 0;
}
