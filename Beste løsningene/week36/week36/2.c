#include <stdio.h>
#include <string.h>


/** Get the character at the given position in a string.
 * @param s Any string.
 * @param pos The position.
 * @return The character at *pos* or -1 if *pos* is invalid.
 * */
char strgetc(char *s, int pos){
	if (pos<0 || pos>=strlen(s))
		return -1;
	return s[pos];
}

int main(){
	char *s = "Hello";
	int  i;
	for (i=-1;  i<=5; i++){
		char c = strgetc(s, i);
		if(c == -1)
			printf("Invalid index: %d\n", i);
		else
			printf("strgetc(\"%s\", %d) gir '%c'.\n",
				s, i, c);
	}
	return 0;
}
