#include <assert.h>

int strcmpx (char *s1, char *s2){
	int x = 0;

	while(s1[x] == s2[x]  &&  s1[x] != 0)
		x++;
	return s1[x] - s2[x];
}


int main(){
	// Some tests.
	// See *man 3 assert* for information about *assert*.
	assert(strcmpx("ahh", "ahh") == 0);
	assert(strcmpx("ahh", "bee") < 0);
	assert(strcmpx("bee", "ahh") > 0);
	return 0;
}
