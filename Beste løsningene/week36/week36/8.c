#include <stdio.h>

int main(){
	int inches;

	while(1) {
		printf("Inches: ");
		scanf("%d", &inches);
		if(inches == 0)
			break;

		int foot = inches/12;
		int finches = inches%12;
		printf("%d inches is %d foot and %d inches.\n", 
			inches, foot, finches);
	}
	return 0;
}

