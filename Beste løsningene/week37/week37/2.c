#include <stdlib.h>
#include <stdio.h>



/** Allocate size*4+1 bytes of memory and fill it with the string "tull"
 * repeated as many times as possible.
 * @return Pointer to the allocated memory area padded with the \0 byte.
 */
char * tulloc(size_t size){
	int times = 4;
	size_t totsize = size*times + 1;
	char *start = (char *) malloc(totsize);
	char *cur = start;
	while(cur < start+(size*times)){
		cur[0] = 't';
		cur[1] = 'u';
		cur[2] = 'l';
		cur[3] = 'l';
		cur += times;
	}
	cur ++;
	*cur = '\0';
	return start;
}


int main(){
	char *x = tulloc(4);
	printf("%s\n", x);
	return 0;
}
