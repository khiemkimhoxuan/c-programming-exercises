/** The error handler used in "5g.c". */
#include <stdio.h>
#include <stdlib.h>


struct Error{
	char *domain; // where did the error occur
	char *msg; // error message
};


/** Set an error.
 * @param domain  Describes where the error occurred. Will normally be the name
 * 		of the function where the error occurred.
 * @param msg  A short string describing the error.
 * */
void
error_set(struct Error **e, char *domain, char *msg){
	*e = (struct Error*) malloc(sizeof(struct Error));
	(*e)->domain = domain;
	(*e)->msg = msg;
}


/** Free memory occupied by an error. */
void
error_free(struct Error **e){
	free(*e);
	*e = 0;
}


/** Print an error. */
void
error_print(struct Error *e){
	fprintf(stderr, "Error in %s: %s\n", e->domain, e->msg);
}


/** Print an error and exit the program. */
void
error_fatal(struct Error *e){
	error_print(e);
	exit(1);
}




/****************************************************************
 * Example
 ****************************************************************/


/** If "n" is higher than 10, raise an error. */
void max_ten(int n, struct Error **e){
	if(n > 10)
		error_set(e, "max_ten", "'n' cannot be higher than 10.");
}


int main(){
	struct Error *e = 0;
	max_ten(20, &e);
	if(e != 0){
		error_print(e);
		error_free(&e);
	}
	else
		printf("max_ten() returned without an error\n");

	return 0;
}
