#include <stdio.h>
#include <ctype.h>


int
main(int argc, char **argv)
{
	if(argc != 2){
		printf("usage: %s <filename>\n", argv[0]);
		return 1;
	}

	// Open file for reading
	FILE *f = fopen(argv[1], "r");
	if(f == NULL){
		perror("open file");
		return 2;
	}

	int words = 0;

    while (fscanf(f, "%*s")!=EOF) {
	/*
		Som forklart p� gruppetimen:

		Her bruker man alst� fscanf som er scanf for en gitt fil.
		"%*s" er formatstringen.

		%s betyr at man leser inn en string, alts� ett ord, til et argument som man gir etter format-stringen.
		Med %*s leser man inn en string, men dropper � legge det i noe argument.
		Alts� betyr * (stjerne) at man dropper � lagre det til noe argument.
		Dermed slipper man � bruke et midlertidig buffer som i 4.c

	*/
        words++;
    }
	fclose(f);
	printf("%d\n", words);
	return 0;
}
