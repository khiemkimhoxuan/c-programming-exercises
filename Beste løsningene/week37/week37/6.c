#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>


/* kommentarene er mest for de som ikke var p� gruppetimen,
eventuelt en repetisjon for de som var der... */

struct Person {
	char *name;
	int age;
};

int parse_cmd(char* cmdline);

/* 
en statisk allokert struktur, den kan alts� ikke vokse.
*/
#define REGSIZE 100
struct Person registry[REGSIZE];

void close_registry() {
    int i;
    for (i=0; i< REGSIZE; i++) {
        if (registry[i].name!=0) {
	    /*
	    pr�v � kommenter ut free og kj�r "make run_memcheck"
	    og deretter med free. Se p� forskjellen.
	    P� denne m�ten kan man sjekke om programmet lekker minne eller ikke.
	    */
            free(registry[i].name);
        }
    }
}

void init_registry() {
    int i;
    for (i=0; i< REGSIZE; i++) {
	// man setter alle elementene i registeret til en initiell verdi.
        registry[i].name = 0;
        registry[i].age = 0;
    }
}

int save_registry(char* filename) {
    FILE* fd = fopen(filename, "w");
    int i;

    int bytes = 0;

    if (fd == NULL) return 0;

    for (i=0; i<REGSIZE; i++) {
        if (registry[i].name==0) {
	    // hvis ikke elementet har et gyldig navn, fortsetter man bare.
            continue;
        }
        else {
            int retval = fprintf(fd, "ADD %s %d\n", registry[i].name, registry[i].age);
	    // man skriver ut registeret i samme syntax som man tar argumenter.
	    // dette gj�r at man kan gjenbruke parse_cmd kommandoen til en viss grad
            if (retval <= 0) return 0;
            bytes+=retval;
        }
    }

    if (0==fclose(fd)) {
        return bytes;
    }
    else {
        return 0;
    }
}

int load_registry(char* filename) {
    close_registry();
    init_registry();

    FILE* fd = fopen(filename, "r");
    if (fd == NULL) return 0;

    printf("\n");

    char cmdline[255];
    while(NULL != fgets(cmdline, sizeof(cmdline), fd)) {
    	// her sender man hver linje man leser fra registeret inn til parse_cmd, alts� samme funksjon
	// som man brukte til � lese inn "user input"
        parse_cmd(cmdline);
    }

    if (0==fclose(fd)) {
        return 1;
    }
    else {
        return 0;
    }

}


int remove_person(char* name) {
    int i;
    int result = 0;
    for (i=0; i<REGSIZE; i++)  {
    //for (i=0; registry[i].name!=0 && i<REGSIZE; i++)  {
    /*
    	Sp�rsm�l: Hvorfor vil ikke overst�ende linje fungere?
	Eventuelt hvorfor kan den feile?

	Svar:
	Det er pga. den bare kj�rer mens registry[i].name!=0..
	Alts� om man fjerner en person som ligger tidlig i registeret, vil
	man ikke f� fjernet folk som ligger bakenfor.
    */
        if (registry[i].name==0) continue;
        if (0 == strcmp(registry[i].name, name)) {
            free(registry[i].name);
            registry[i].name = 0;
            registry[i].age = 0;
            result = 1;
        }
    }
    return result;

}

int add_person(char* name, int age) {
    int i;
    struct Person* p = 0;
    for (i=0; registry[i].name!=0 && i<REGSIZE; i++) 
        ;

    if (i!=REGSIZE) {
        p = &registry[i];
	/*
	Som sagt har man allokert en minnestruktur til registeret.
	Her henter man ut en peker til det i-te elementet.
	S� p peker alts� til det i-te elementet i registry variabelen.
	*/
    }
    else {
        return 0;
    }

    //printf("Using index %d\n", i);

    p->name = strdup(name); //strdup lager en kopi av stringen.. Denne m� man senere kalle free() p�.
    p->age = age;
    /*
	Dette kunne man ogs� gjort p� f�lgende m�te:
	//registry[i].name = strdup(name);
	//registry[i].age = age;

	Dette er alts� ekvivalente m�ter � gj�re det p�.
    */

    return 1;
}

/*
	parse_cmd funksjonen under bruker sscanf en del.
	Du kan finne informasjon om den i man sidene (man 3 sscanf).

	Det er kanskje enklest � forklare den med et eksempel.

    	sscanf(cmdline, "LOAD %s", arg))
	Dette scanner variabelen cmdline for m�nsteret "LOAD %s"
	sscanf returnerer antall argument den finner.
	S� dersom dette "treffer", s� returnerer den 1 og legger
	ett ord i arg variabelen.

    	Tilsvarende litt lenger ned i koden finner man:
	    else if (2 == sscanf(cmdline, "ADD %30s %d", arg, &age)) {

	Her sjekker man om returverdien er 2. Alts� at to argumenter har truffet.
	"ADD" i formatstringen er ikke en del av antall treff.
	%30s betyr en string/ord p� maks 30 bokstaver.
	%d er et tall, tilsvarende printf.

*/
int parse_cmd(char* cmdline) {
    char arg[31];
    int age;
    if (1 == sscanf(cmdline, "LOAD %s", arg)) {
        int result = load_registry(arg);
        printf("loading file '%s'... %s\n", arg, result==1 ? "OK" : "FAILURE");
    }
    else if (2 == sscanf(cmdline, "ADD %30s %d", arg, &age)) {
        if (age > 200) { 
            printf("Invalid age!\n");
        }
        int result = add_person(arg, age);
        printf("adding person %s... %s\n", arg, result==1 ? "OK" : "FAILURE");
    }
    else if (1 == sscanf(cmdline, "REMOVE %s", arg)) {
        int result = remove_person(arg);
        printf("removing person %s... %s\n", arg, result==1 ? "OK" : "FAILURE");
    }
    else if (1 == sscanf(cmdline, "SAVE %s", arg)) {
        int result = save_registry(arg);
        printf("Saving to file '%s'... ", arg);
        if (result==0) {
            printf("FAILURE\n");
        }
        else {
            printf("OK, wrote %d bytes.\n", result);
        }
    }
    else if (0 == strcmp(cmdline, "QUIT\n")) {
        printf("Quitting ...\n");
        return 0;
    }
    else if (0 == strcmp(cmdline, "\n")) {
        printf("Skipping empty command.\n");
    }
    else {
        cmdline[strlen(cmdline)-1] = 0;
        printf("Could not understand command '%s'.\n", cmdline);
    }
    
    return 1;
}

int main(int argc, char **argv) {
    init_registry();

    while(1) {
        char cmdline[255];

        printf("> ");
        bzero(cmdline, sizeof(cmdline));
        char* result = fgets(cmdline, sizeof(cmdline), stdin);

        if (result==(char*)EOF || result==NULL) break;

        if (0==parse_cmd(cmdline)) break;
    }

    close_registry();
    printf("\nGoodbye!\n");

    return EXIT_SUCCESS;
}

