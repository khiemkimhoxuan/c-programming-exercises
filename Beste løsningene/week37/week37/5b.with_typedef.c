
/** This file defines the data structures for a Person "class" in C.
 * Note that the getAge and getName functions is somewhat useless because it is
 * easier to just access the struct directly. */
#include <stdio.h>


typedef int age_t;


/** Data structure with information about a person. */
struct Person{
	char *name;
	age_t age;
};
typedef struct Person * PersonP;



/** Initialize the person structure. */
void
person_init(PersonP p){
	p->name = 0;
	p->age = -1;
}


/** Get the name of a person. */
char *
person_getName(PersonP p){
	return p->name;
}


/** Get the age of a person. */
int
person_getAge(PersonP p){
	return p->age;
}


/** Set the name of a person. */
void
person_setName(PersonP p, char *name){
	p->name = name;
}


/** Set the age of a person. */
void
person_setAge(PersonP p, int age){
	p->age = age;
}


int main(){
	struct Person p;
	person_init(&p);
	person_setName(&p, "Amy");
	person_setAge(&p, 23);
	printf("name:%s   age:%d\n", person_getName(&p), person_getAge(&p));
	return 0;
}
