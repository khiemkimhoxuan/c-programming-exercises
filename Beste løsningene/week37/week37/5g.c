/** This file defines the data structures for a Person "class" in C.
 * Note that the getAge and getName functions is somewhat useless because it is
 * easier to just access the struct directly. */
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>




/***********************************************************
 * Error handling (the error "class")
 * see errorhandler.c for basic usage examples.
 **********************************************************/

struct Error{
	char *domain; // where did the error occur
	char *msg; // error message
};


/** Set an error.
 * @param domain  Describes where the error occurred. Will normally be the name
 * 		of the function where the error occurred.
 * @param msg  A short string describing the error.
 * */
void
error_set(struct Error **e, char *domain, char *msg){
	*e = (struct Error*) malloc(sizeof(struct Error));
	(*e)->domain = domain;
	(*e)->msg = msg;
}


/** Free memory occupied by an error. */
void
error_free(struct Error **e){
	free(*e);
	*e = 0;
}


/** Print an error. */
void
error_print(struct Error *e){
	fprintf(stderr, "Error in %s: %s\n", e->domain, e->msg);
}


/** Print an error and exit the program. */
void
error_fatal(struct Error *e){
	error_print(e);
	exit(1);
}



/***********************************************************
 * Person "class"
 **********************************************************/


/** Data structure with information about a person. */
struct Person{
	char *name;
	int age;
};


/** Initialize the person structure. */
void
person_init(struct Person *p){
	p->name = 0;
	p->age = -1;
}


/** Get the name of a person. */
char *
person_getName(struct Person *p){
	return p->name;
}

/** Set the name of a person. */
void
person_setName(struct Person *p, char *name, struct Error **e){
	if(strlen(name) > 30)
		error_set(e, "person_setName",
			"Name cannot be more than 30 characters.");
	else
		p->name = name;
}

/** Check if the name of a person is defined.
 * @return 1 for true and 0 for false.
 * */
int
person_nameIsSet(struct Person *p){
	return p->name != 0;
}


/** Get the age of a person. */
int
person_getAge(struct Person *p){
	return p->age;
}

/** Set the age of a person. */
void
person_setAge(struct Person *p, int age, struct Error **e){
	if(age > 200)
		error_set(e, "person_setAge", "Age cannot be above 200");
	else
		p->age = age;
}

/** Check if the age of a person is defined.
 * @return 1 for true and 0 for false.
 * */
int
person_ageIsSet(struct Person *p){
	return p->age != -1;
}




/***********************************************************
 * Tests
 **********************************************************/
void test(){
	struct Person p;
	struct Error *e = 0;

	person_init(&p);
	assert(p.name == 0);
	assert(p.age == -1);
	assert(person_ageIsSet(&p) == 0);
	assert(person_nameIsSet(&p) == 0);

	person_setName(&p, "a", &e);
	assert(e == 0);
	assert(person_nameIsSet(&p));
	assert(strcmp(person_getName(&p), "a") == 0);
	person_setName(&p, "012345678901234567890123456789X", &e);
	assert(e != 0);
	error_free(&e);

	person_setAge(&p, 23, &e);
	assert(e == 0);
	assert(person_ageIsSet(&p));
	assert(person_getAge(&p) == 23);
	person_setAge(&p, 201, &e);
	assert(e != 0);
	error_free(&e);
}





/***********************************************************
 * Example program
 **********************************************************/
int main(int argc, char **argv){
	#ifndef NDEBUG
		test();
	#endif


	if(argc != 3){
		fprintf(stderr, "usage: %s <name> <age>\n", argv[0]);
		return 1;
	}

	struct Error *e = 0;
	struct Person p;
	person_init(&p);
	person_setName(&p, argv[1], &e);
	if(e != 0)
		error_fatal(e);
	person_setAge(&p, atoi(argv[2]), &e);
	if(e != 0)
		error_fatal(e);

    printf("OK\n");
	return 0;
}
