#include <stdio.h>

void settall(int* p) {
    *p = 500;
}

int main(int argc, char** argv) {
    int tall[] = { 1,2,3,4,5,6,7,8,9, 0};
            //     0 1 2 3 4 5
    int *p = tall;
    int i;

    settall(&p[5]);

    for (i=0; tall[i]!=0; i++) {
        printf("%d er %d\n", i, tall[i]);
    }

    return 0;

}

