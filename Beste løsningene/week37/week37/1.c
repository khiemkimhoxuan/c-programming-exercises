#include <assert.h>


/** Calculate the sum of *a* and *b* and place the result
 * in *result*. */
void sum(int a, int b, int *result){
	*result = a + b;
}


int main(){
	int r;
	sum(2, 3, &r);
	assert(r == 5);
	return 0;
}
