#include <stdio.h>


int main(){
	int i;
	short arr[] = {2, 1, -1, 10, 22, 21};
	int arrlen = 6;
	short *p;

	printf("** Original solution:\n");
	for(i=0; i<arrlen; i++)
		printf("[%d]: %d\n", i, arr[i]);


	printf("** For loop with pointers:\n");
	for(p=arr; p<(arr+arrlen); p++){
		i = p - arr;
		printf("[%d] %d\n", i, *p);
	}

	printf("** For loop with pointers, version 2:\n");
    for (i=0, p=arr; i<arrlen; i++, p++) {
		printf("[%d] %d\n", i, *p);
    }

	return 0;
}
