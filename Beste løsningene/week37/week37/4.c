#include <stdio.h>
#include <ctype.h>


int
main(int argc, char **argv)
{
	if(argc != 2){
		printf("usage: %s <filename>\n", argv[0]);
		return 1;
	}

	// Open file for reading
	FILE *f = fopen(argv[1], "r");
	if(f == NULL){
		perror("open file");
		return 2;
	}

	int bufsize = 1000;
	char buf[bufsize];
	int words = 0;

	// Read file line by line and count number of words on each line.
    char prev = 0;
	while(fgets(buf, bufsize, f) != NULL){
		int i;

		// Count number of words on in the current line.
		// Every line ends with a whitespace, so if words are separated
		// by ' ', we should be able to count the number of words by counting
		// spaces + the newline (isspace('\n') == 1). But some words might be
		// separated by more than one space, so we need to keep track of
		// previous spaces..
		for(i=0; buf[i] != '\0'; i++){
            //printf("%c", buf[i]);
			if(isspace(buf[i]) && !isspace(prev)) {
                //printf("\n");
				words ++;
            }
			prev = buf[i];
		}
	}
	fclose(f);
	printf("%d\n", words);
	return 0;
}
