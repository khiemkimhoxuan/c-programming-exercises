#include "mlog.h"
#include <string.h>


int mlog_level = MLOG_ERROR;

void
mlog_config(int level)
{
	mlog_level = level;
}

int
mlog_msg(int level, const char *format, ...)
{
	if(mlog_level > level)
		return 0;

	va_list ap;
	va_start(ap, format);

    /*  The C book */
	char fmt[strlen(format) + 5]; // "** \n\0" == 5
	sprintf(fmt, "** %s\n", format);
	int ret = vfprintf(stderr, fmt, ap);

	va_end(ap);
	return ret;
}
