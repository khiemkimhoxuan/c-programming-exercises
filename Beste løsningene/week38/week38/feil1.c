#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#define max_lines 3

char* tekst[max_lines];

int lagre(char* buffer) {
    static int index = 0;

    char* nl = strchr(buffer, '\n');
    if (nl) *nl = '\0';

    if (index<max_lines) {
        printf("Lagrer '%s' på index %d\n", buffer, index);
        /*
           // de tre følgende linjer fungerer:

        char *cpy = malloc(strlen(buffer) + 1);
        strcpy(cpy, buffer);
        tekst[index++] = cpy;

        // original kode:
        tekst[index] = buffer;
        // dette ville bare kopiert en peker.. og kanskje kræsjet programmet
        // pekeren som ble kopiert var altså buf[512] i lesfil() sitt skop eller "minneområde".
          */
        tekst[index] = malloc(strlen(buffer)+1);
        strcpy(tekst[index], buffer);
        index++;
        return 1;
    }
    else {
        printf("Tom for plass. Index var %d\n\n", index);
        return 0;
    }
}

void lesfil(FILE* f) {
    char buf[512];
    while(fgets(buf, sizeof(buf), f)==buf) {
        printf("buf adresse var %p\n", buf);
        if (!lagre(buf)) break;
    }
}

void skrivlinjer(void) {
    int i;

    for (i=0; i<max_lines; i++) {
        printf("adresse %p, Linje %d: %s\n", tekst[i], i+1, tekst[i]);
    }
}

int main(int argc, char** argv) {

    FILE* f = fopen("/dev/stdin", "r");

    assert(f);
    lesfil(f);

    skrivlinjer();

    assert(0 == fclose(f));


    return 0;
}
