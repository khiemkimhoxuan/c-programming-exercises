#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>

enum { MLOG_ERROR=1, MLOG_DEBUG, MLOG_INFO };

int mlog_level = MLOG_ERROR;

void mlog_config(int level)
{
	mlog_level = level;
}

int mlog_msg(const char* file, const char* func, int line, int level, const char *format, ...)
{
	if(mlog_level > level)
		return 0;

	va_list ap;
	va_start(ap, format);

    fprintf(stderr, "** File %s, function %s, line %d: ", file, func, line);
	int ret = fprintf(stderr, format, ap);

	va_end(ap);
	return ret;
}

#define mlog(...) mlog_msg(__FILE__, __func__, __LINE__, __VA_ARGS__)
                        // makro     makro      makro      makro

void
try_mlog()
{
	int a = 10;
    mlog(MLOG_DEBUG, "The value of a is %d.\n", a);
    mlog_msg(__FILE__, __func__, __LINE__, MLOG_DEBUG, "The value .. is %d\n", a);

    mlog(MLOG_ERROR, "Crap something is wrong here!\n");
	mlog(MLOG_INFO, "Hello world:)\n");
}

int main(int argc, char** argv) {
    //assert(0 == MLOG_DEBUG);
    try_mlog();
    return 0;
}


