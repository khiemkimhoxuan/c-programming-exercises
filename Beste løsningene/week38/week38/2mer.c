/*
 * 
 *
 *
 */

#include <stdio.h>
int main(){

    char *s = "hello world"; // skaffer bare en pointer til en read-only minneadresse   

    char ss[] = "hello world"; // lager en kopi til ss fra en read-only minneadresse, så
                               // derfor kan vi skrive til den.
                               //
                               // Mer detaljer?
                               //
                               // Prøv gcc -S -Wall -g 2mer.c
                               // og sjekk så 2mer.s filen.
    *ss = 'H';

    return 0;
}


