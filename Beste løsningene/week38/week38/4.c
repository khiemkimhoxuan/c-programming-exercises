#include <string.h>
#include <stdio.h>
#include <stdlib.h>


int
main(int argc, char **argv)
{
	if(argc != 4){
		printf("Usage: %s <name> <age> <outfile>\n", argv[0]);
		return 1;
	}
	char *name = argv[1]; // 
    // ./4 per 15 ut.txt
    // *name = 'p'
    // name[0] == 'p' == argv[1][0] 
    // name[1] == 'e' == argv[1][1]
    // name[2] == 'r' == argv[1][2]
	char *sage = argv[2];
	char *filename = argv[3];
	int age = atoi(sage);

	// Create the info string
	char *fmt = "Hello %s, you are %d years old";
	int infosize = strlen(name) + strlen(sage) + strlen(fmt) - 4;
	char info[infosize];
	sprintf(info, fmt, name, age);

	// Write string to file
	FILE *f = fopen(filename, "w");
	if(f == NULL){
		fprintf(stderr, "** Error while opening %s.\n", filename);
		perror("open file");
		return 2;
	}
	int items_written = fwrite(info, sizeof(char), infosize, f);
	if(items_written != infosize){
		if(ferror(f)){ // an error occurred
			fprintf(stderr, "** Error while writing %s.\n", filename);
			perror("write to file");
			return 3;
		}
		else{
			// Not all bytes where written to file. We could have
			// tried to continue..
			fprintf(stderr, "** Could not write the entire info string to %s\n",
					filename);
			return 4;
		}
	}

    fclose(f);
    return 0;
}
