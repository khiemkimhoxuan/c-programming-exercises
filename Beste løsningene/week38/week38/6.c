#include "mlog.h"
#include <stdio.h>
#include <assert.h>

void
try_mlog()
{
	int a = 10;
    mlog_msg(MLOG_DEBUG, "The value of a is %d.\n", a);
    mlog_msg(MLOG_ERROR, "Crap something is wrong here!\n");
	mlog_msg(MLOG_INFO, "Hello world:)\n");
}


int
main(int argc, char **argv)
{
    //assert(0 == MLOG_ERROR);
	fprintf(stderr, "Default loglevel (MLOG_ERROR):\n");
	try_mlog();

	mlog_config(MLOG_INFO);
	fprintf(stderr, "\nloglevel MLOG_INFO:\n");
	try_mlog();

	mlog_config(MLOG_DEBUG);
	fprintf(stderr, "\nloglevel MLOG_DEBUG:\n");
	try_mlog();
	return 0;
}
