/** A very simple log system with lots of room for improvements. */

#ifndef MLOG_H
#define MLOG_H

#include <stdio.h>
#include <stdarg.h>

#define MLOG_DEBUG 0
#define MLOG_INFO 1
#define MLOG_ERROR 2


/* Defined in mlog.c. We declare it as *extern* so it will be
 * available to everyone using this header file. */
extern int mlog_level;


/** Configure mlog.
 * @param level  The log level. Only messages with greater or equal level
 * 		are printed.
 */
void
mlog_config(int level);


/** Log a message.
 * Works exactly like *printf*, except for the *level* parameter.
 * @param level  The log level. If level is smaller than the level configured
 * 		with mlog_config(), the message will not be printed.
 * @param format  See "man 3 printf".
 * @return See "man 3 printf".
 */
int
mlog_msg(int level, const char *format, ...);


#endif
