#include <stdio.h>
#include <string.h>


int
main()
{
	int bufsize = 500;
	char buf[bufsize];

	while(1){
		// read one line of keyboard input
		printf("Filename: ");
		if(fgets(buf, bufsize, stdin) == NULL){
			perror("read filename");
			return 1; // Klarte ikke lese inn filnavn
		}

		if(strncmp(buf, "QUIT", 4) == 0)
			break; // avslutt om quit... brekker altså ut av while loopen.
		char *filename = buf; // ny peker.

		// Remove \n
		int filenamelen = strlen(filename);
		if(filename[filenamelen-1] == '\n')
			filename[filenamelen-1] = '\0';

		// Don't accept empty *filename*
		if(strlen(filename) == 0)
			continue;

		// Open *filename* for reading
		FILE *f = fopen(filename, "r");
		fprintf(stderr, "Reading: \"%s\"\n", filename);
		if(f == NULL){
			perror("open file");
			return 2;
		}

		// Print every other line in *filename*
		int i = 0;
		while(fgets(buf, bufsize, f) != NULL){
			if(i % 2 == 0)
				printf("%4d: %s", i, buf);
			i ++;
		}
		if(ferror(f) != 0){
			perror("read line");
			return 3;
		}

        fclose(f);
	}

    return 0;
}
