/** A updated version of 5.c. No functional improvements, but the code is
 * separated into one function for each functionality (without taking this to
 * the extreme).
 *
 * This program is far from perfect. Some of the more prominent is that it has
 * bad error handling and uses fprintf instead of some kind of logging system.
 * */
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>


typedef unsigned int uint;


/** Sleep for a random number of seconds (between 0 and *max*). */
void
sleeprandom(int max)
{
	srand(getpid()); // we do not use time because this would give all processes the same seed (provided the start within the same second)
	int w = rand() % max+1;
	fprintf(stderr, "[%d] sleeping for %d seconds.\n", getpid(), w);
	sleep(w);
}


/** Calculate the sum of start+start+1...+start+n (n<end).
 * @return The calculated sum.
 * */
uint
sum(uint start, uint end)
{
	// Calculate the sum
	uint s = 0;
	uint i;
	for(i=start; i<end; i++)
		s += i;
	return s;
}


/** Fork off a child process which calculates the sum of all numbers between
 * *start* and *end* and writes the result to *filename*. */
void
sumfork(uint start, uint end, const char *filename)
{
	if(fork() == 0){
		fprintf(stderr, "[%d] starting\n", getpid());

		int s = sum(start, end);
		sleeprandom(5);

		// Write the sum to file (with error handling)
		FILE *fp = fopen(filename, "w");
		if(fp == NULL){
			fprintf(stderr, "[%d] Failed to open %s for writing.",
				getpid(), filename);
			exit(1); // non-zero exit value signals an error
		}
		if(fprintf(fp, "%d", s) < 0){
			fprintf(stderr,
				"[%d] Writing number to file failed\n",
				getpid());
			exit(2);
		}
		fclose(fp);

		exit(0);
	}
}


/** Read and return an uint from a file. */
uint
read_uint_from_file(const char *filename)
{
	FILE *fp = fopen(filename, "r");
	if(fp == NULL){
		fprintf(stderr, "Failed to open %s for reading\n", filename);
		exit(-1);
	}

	char buf[15];
	fread(buf, 1, 15, fp);
	if(ferror(fp) != 0){
		fprintf(stderr, "Failed to read from %s\n", filename);
		exit(-2);
	}
	fclose(fp);

	return (uint) atol(buf);
}


int
main()
{
	sumfork(1, 1000, "sum1.txt");
	sumfork(1001, 2000, "sum2.txt");
	sumfork(2001, 3000, "sum3.txt");

	// wait for the 3 child processes to finish.
	int status, i, childpid;
	int exitstatus = 0;
	for(i=0; i<3; i++){
		childpid = wait(&status);
		fprintf(stderr, "Child %d terminated with status %d\n", childpid, status);

		// Make the exit code contain information about the number of errors.
		// This is just an example of how to use exit status for something
		// useful.
		if(status != 0)
			exitstatus ++;
	}

	// Calculate the sum by reading the output files from the child processes
	int sum = read_uint_from_file("sum1.txt");
	sum += read_uint_from_file("sum2.txt");
	sum += read_uint_from_file("sum3.txt");
	printf("Sum: %d\n", sum);

	return exitstatus;
}
