#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

void sleeprandom(int max) {
	srand(getpid()); 
	int w = rand() % max+1;
	fprintf(stderr, "[%d] sleeping for %d seconds.\n", getpid(), w);
	sleep(w);
}

void sum(int start, int end) {
    int i;
    int s = 0;
    if (fork()) return;

    printf("child running...\n");
    sleeprandom(5);
    for (i=start; i<end; i++)
        s+=i;

    printf("result %x\n", s);
    exit(s);
}

int main(int argc, char** argv) {

    int status;
    int val = 0;
    int i = 0;
    sum(1, 1000);
    sum(1001, 2000);
    sum(2001, 3000);

    for (i=0; i<3; i++) {
        int pid = wait(&status);
        assert(WIFEXITED(status));
        int result = WEXITSTATUS(status);
        val+=result;
        printf("val %x %x\n", result, status);
    }
    printf("answer was %d\n", val);

    return 0;
}
