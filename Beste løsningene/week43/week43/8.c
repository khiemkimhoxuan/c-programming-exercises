#include <fcntl.h> // open, write ..
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h> // perror
#include <string.h> // strlen


int main(int argc, char **argv){
	// could have used creat(), but using open to make a better example
	int fd = open("test.txt",
			O_WRONLY | O_CREAT | O_TRUNC, // mode
            // 001       010     100  = 111
			S_IRWXU); // permissions
	if(fd == -1){
		perror("open() failed");
		return 1;
	}

	char *msg  = "hello world";
	int r = write(fd, msg, strlen(msg));
	if(r == -1)
		perror("write() failed.");
	else
		printf("Wrote %d bytes to file.\n", r);
	close(fd);
	return 0;
}
