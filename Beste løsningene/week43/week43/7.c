#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <assert.h>
#include <string.h>

int main(int argc, char** argv) {

    int p = fork();
    assert(p!= -1);

    int child = p==0;

    printf("pid %d running. is child: %s\n", getpid(), child ? "Yes" : "No");

    FILE* fd = fopen("./test", "w");
    if (fd==NULL) {
        printf("pid %d could not open file...\n", getpid());
        exit(1);
    }
    else {
        printf("could open file %p...\n", fd);
    }
    printf("ferror ok: %d\n", 0==ferror(fd));

    fflush(stdout);
    if (child) {
        // barn : skriv bbb
        char s[] = "bbbbbbbbbb\n";
        sleep(2);
        fseek(fd, 0L, SEEK_END);
        fwrite(s, strlen(s), sizeof(char), fd);
        //fflush(fd);
        assert(0 == fclose(fd));
    }
    else {
        // forelder : skriv aaa
        char s[] = "aaa\n";
        fseek(fd, 0L, SEEK_END);
        fwrite(s, strlen(s), sizeof(char), fd);
        printf("waiting for child...\n");
        fflush(stdout);
        int status;
        wait(&status);

        fflush(fd);
        assert(0 == fclose(fd));
    }

    return 0;
}

