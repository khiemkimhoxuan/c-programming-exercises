
# Oppgave 1.B

def fcfs(prev, tracks):
    for x in tracks:
        yield x

def scan(prev, tracks):
    pieces = [[x for x in tracks if x>=prev], [x for x in tracks if x<prev]]

    pieces[0].sort()
    pieces[1].sort()
    pieces[1].reverse()

    full = pieces[0]
    full.extend(pieces[1])
    for x in full:
        yield x

import math

def measure(name, prev, cb, cost_per_track=None):
    if cost_per_track==None:
        cost_per_track = lambda x : 10*x
    tracks = [99, 55, 43, 78, 4, 11, 89, 67, 1, 98, 45, 88, 60, 30, 77]
    totcost = 0
    ms_cost = 0
    print "\n*** Doing %s" % (name)
    for take in cb(prev, tracks):
        cost = math.fabs(0.0 + take-prev)
        totcost +=cost
        print "from %2d to %2d -- %2d cost" % (prev, take, cost)
        prev = take
        ms_cost += 1
        ms_cost += cost_per_track(cost)
        ms_cost += ((60*1000.0) / 7500.0) * 0.5
    #print "Total cost %d" % (totcost)
    print "MS total cost %f ms or %f s" % (ms_cost, ms_cost/1000.0)

    return ms_cost

cpt = lambda x: 10*math.sqrt(x)

s = measure("SCAN", 49, scan,cpt)
f = measure("FCFS", 33, fcfs,cpt)

print "fcfs/scan = %f"  % (f/s)

