#include <stdio.h>
#include <assert.h>

// kcalc er en grei hex kalkulator.

void printbin(int x, int numbits) {
    int i;
    numbits--;
    assert(numbits>=0);

    // 12345678
    // 10001111 >> 6
    //   123456
    // fjerner altså de 6 laveste bits
    // da står vi igjen med:
    // 10 & 1
    //  ^ --- & her henter ut den aller laveste bitten
    //
    //  numbits = 8
    //  numbits--..
    //  numbits = 7
    //
    //  (x >> 7) & 1
    //
    //  (x >> 6) & 1
    //  ... osv ...
    //  (x >> 0) & 1
    //
    for (i=numbits; i>=0; i--) {
        int bit = (x >> i) & 1;
        printf("%c", bit==0 ? '0' : '1');
    }
}

void bit_eat(int* dest, int inbits, int numbits) {
    /*
     * Gjennomgått på gruppetimen. 
     *
     * Prøv å løse den selv!
     *
     * Den skal altså sette inn ny bits til et tall.
     *
     * Eks: 
     *
     * *dest = 101 bin
     * inbits = 11 bin
     * numbits = 2 decimal
     *
     * da skal *dest til slutt bli
     * 101 11
     *     ^^ disse er de nye bittene
     */

    // 00101   << 2
    // 10100
    //
    // numbits = 2
    // (1<<2) -1
    // 1<<2 == 100
    // 100 - 1
    // 011
    // 11
    //
    // 1111
    // &
    // 0110
    // ====
    // 0110
    //
    // 1 | 0 = 0... 
    // 1 & 0 = 0

    // 1010
    // |
    // 0100
    // ====
    // 1110
}

int main(int argc, char** argv) {

    int x = 0;

    bit_eat(&x, 1, 1);
    bit_eat(&x, 1, 1);
    bit_eat(&x, 1, 1);
    bit_eat(&x, 1, 1);

    bit_eat(&x, 1, 1);
    bit_eat(&x, 1, 1);
    bit_eat(&x, 1, 1);
    bit_eat(&x, 1, 1);

    printf("x er %x, ok? %d \n", x, x==0xff);

    x = 0;

    bit_eat(&x, 1, 1);
    bit_eat(&x, 1, 1);
    bit_eat(&x, 1, 1);
    bit_eat(&x, 1, 1);

    bit_eat(&x, 0, 1);
    bit_eat(&x, 0, 1);
    bit_eat(&x, 0, 1);
    bit_eat(&x, 0, 1);
    printf("x er %x, ok? %d \n", x, x==0xf0);

    x = 0;

    bit_eat(&x, 0, 1);
    bit_eat(&x, 0, 1);
    bit_eat(&x, 0, 1);
    bit_eat(&x, 0, 1);

    bit_eat(&x, 1, 1);
    bit_eat(&x, 1, 1);
    bit_eat(&x, 1, 1);
    bit_eat(&x, 1, 1);
    printf("x er %x, ok? %d \n", x, x==0x0f);

    x = 0;
    bit_eat(&x, 3, 2);
    bit_eat(&x, 3, 2);
    bit_eat(&x, 0, 2);
    bit_eat(&x, 0, 2);
    printf("x er %x, ok? %d \n", x, x==0xf0);


    x = 0;
    bit_eat(&x, 0, 2);
    bit_eat(&x, 0, 2);
    bit_eat(&x, 3, 2);
    bit_eat(&x, 3, 2);
    printf("x er %x, ok? %d \n", x, x==0x0f);

    enum {
        b00 = 0, b01 = 1, b10 = 2, b11 = 3
    };

    x = 0;
    bit_eat(&x, b11, 2);
    bit_eat(&x, b10, 2);
    bit_eat(&x, b01, 2);
    bit_eat(&x, b00, 2);
    printf("x er %x, ok? %d \n", x, x==228);

    printbin(0xf0, 8);
    printf("\n");
    printbin(0x0f, 8);
    printf("\n");
    printbin(228, 8);
    printf("\n");
    return 0;
}


