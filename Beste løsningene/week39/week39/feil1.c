#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
struct element {
    int verdi;
    struct element* neste;
};

// Her er det mange feil. Prøv å finne de.
//
// Tips:
//
// kompiler med -g og -Wall for debugging og advarsler.
//
// Dette ble gjennomgått på gruppetimen. Du kan ta kontakt
// med ivarref@student.uio.no om du har spørsmål...

void legg_til(struct element* e, int verdi) {

    // finn siste element
    while (e->neste) {
        printf("inne i while\n");
        e = e->neste;
    }

    // legg til nytt element
    e = malloc(sizeof(struct element));
    e->verdi== verdi;
};
void print_liste(struct element* e) {
    printf("test\n");
    while(e->neste) {
        printf("verdien var %d\n", e->verdi);
        e = e->neste;
    }
};

int main(int argc, char** argv) {
    struct element* e;
    legg_til(e, 123);
    legg_til(e, 456);
    legg_til(e, 789);
    legg_til(e, 101112);

    print_liste(e);
    return 0;
}
